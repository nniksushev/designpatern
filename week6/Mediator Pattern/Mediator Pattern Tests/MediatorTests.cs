﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mediator_Pattern;

namespace Mediator_Pattern_Tests
{
    [TestClass]
    public class MediatorTests
    {
        [TestMethod]
        public void CreateMediator()
        {
            IMediator mediator = new StockMediator();

            Assert.IsNotNull(mediator);
        }

        [TestMethod]
        public void AddColl()
        {
            IMediator mediator = new StockMediator();
            Colleague col = new PeterLynch(mediator);
            mediator.AddColleague(col);

            Assert.AreEqual(1, mediator.GetColleagueCount());

        }

        [TestMethod]
        public void GetBuyStock()
        {
            IMediator mediator = new StockMediator();

            Assert.AreEqual(2, mediator.GetStockOfferings()[0].Count);
        }

        [TestMethod]
        public void BuyAStock()
        {
            IMediator mediator = new StockMediator();
            mediator.OnStockChange = () => { };
            mediator.BuyOffer(mediator.GetStockOfferings()[0][0], 3);
            Assert.AreEqual(1, mediator.GetStockOfferings()[0].Count);
        }

        [TestMethod]
        public void BuyAStockAndGetSale()
        {
            IMediator mediator = new StockMediator();
            mediator.OnStockChange = () => { };
            StockOffer of = mediator.GetStockOfferings()[0][0];
            StockOffer toBuy = new StockOffer(of.StockShares - 2, of.StockSymbol, of.ColleagueCode);

            mediator.BuyOffer(toBuy, 3);
            Assert.AreEqual(2, mediator.GetStockOfferings()[0].Count);
            Assert.AreEqual(2, of.StockShares); //because the of is manipulated in mediator, but the refernce stays
            
        }


        [TestMethod]
        public void GetSellStock()
        {
            IMediator mediator = new StockMediator();

            Assert.AreEqual(3, mediator.GetStockOfferings()[1].Count);
        }
    }
}
