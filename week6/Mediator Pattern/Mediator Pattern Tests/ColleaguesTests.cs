﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mediator_Pattern;
namespace Mediator_Pattern_Tests
{
    [TestClass]
    public class ColleaguesTests
    {
        [TestMethod]
        public void CreateColleagueBengji()
        {
            Colleague ben = new BenjaminGraham(null);
            Assert.AreEqual("Benjamin Graham", ben.Name);
        }

        [TestMethod]
        public void CreateColleaguePeterLynch()
        {
            Colleague pl = new PeterLynch(null);
            Assert.AreEqual("Peter Lynch", pl.Name);
        }

        [TestMethod]
        public void CreateColleagueWarrenBuffet()
        {
            Colleague wb = new WarrenBuffet(null);
            Assert.AreEqual("Warren Buffet", wb.Name);
        }

        [TestMethod]
        public void AddColToMediator()
        {
            IMediator mediator = new StockMediator();
            Colleague ben = new WarrenBuffet(mediator);
            
            Assert.AreEqual(0, ben.ColleagueCode);
            ben.RegisterToMediator();
           
            Assert.AreEqual(2, ben.ColleagueCode);
            Colleague wb = new WarrenBuffet(mediator);
            Assert.AreEqual(0, wb.ColleagueCode);
            wb.RegisterToMediator();
            Assert.AreEqual(3, wb.ColleagueCode);
           

        }

        [TestMethod]
        public void ChangeColNumber()
        {
            IMediator mediator = new StockMediator();
            Colleague ben = new WarrenBuffet(mediator);

            ben.SetCollCode(2);
            Assert.AreEqual(2, ben.ColleagueCode);


        }

        [TestMethod]
        public void GetStockFromMediator()
        {
            IMediator mediator = new StockMediator();
            Colleague ben = new WarrenBuffet(mediator);

            Assert.AreEqual(2, ben.GetStock()[0].Count);
            Assert.AreEqual(3, ben.GetStock()[1].Count);
           


        }

        /// <summary>
        /// Since the mediator is never set
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetStockFromNoMediator()
        {
          
            Colleague ben = new WarrenBuffet(null);

            Assert.AreEqual(3, ben.GetStock()[0].Count);
            Assert.AreEqual(2, ben.GetStock()[1].Count);



        }

        /// <summary>
        /// Since the mediator is never set
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void BuyFromNoMediator()
        {

            Colleague ben = new WarrenBuffet(null);

            ben.BuyOffer(new StockOffer(3, "test stcok", 3));



        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void BuyFromMediatorNotExisting()
        {

            IMediator mediator = new StockMediator();
            Colleague ben = new WarrenBuffet(mediator);

            ben.BuyOffer(new StockOffer(3, "test stcok", 3)); // since it does not exist it is added
            Assert.AreEqual(4, ben.GetStock()[0].Count);


        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void BuyFromMediatorExisting()
        {

            IMediator mediator = new StockMediator();
            Colleague ben = new WarrenBuffet(mediator);

            ben.BuyOffer(ben.GetStock()[0][0]); // since it does not exist it is added
            Assert.AreEqual(2, ben.GetStock()[0].Count);


        }
    }
}
