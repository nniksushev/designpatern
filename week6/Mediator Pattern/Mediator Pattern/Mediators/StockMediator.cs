﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator_Pattern
{
    public class StockMediator : IMediator
    {
        private List<Colleague> colleagues; //List of all colleagues
        private List<StockOffer> stockBuyOffers; //The stock buy offers available - list
        private List<StockOffer> stockSellOffers; //The stock sell offers available - list
        private static int colleagueCodes = 0; //the unique identifier for each colleague.
        private Action onStockChange;//Event to update on stock change

        /// <summary>
        /// Property for assigning and getting the on stock change action
        /// </summary>
        public Action OnStockChange
        {
            get
            {
                return onStockChange;
            }
            set
            {
                onStockChange = value;

            }
        }
        public StockMediator()
        {
            colleagues = new List<Colleague>();
            stockBuyOffers = new List<StockOffer>();
            stockSellOffers = new List<StockOffer>();

            //Init buy stocks
            stockBuyOffers.Add(new StockOffer(1000, "Berkshire Hathaway", 1));
            stockBuyOffers.Add(new StockOffer(1000, "LynchEP", 2));
            stockBuyOffers.Add(new StockOffer(1000, "MSFT", 3));
            //Init sell stocks
            stockSellOffers.Add(new StockOffer(1000, "Berkshire Hathaway", 1));
            stockSellOffers.Add(new StockOffer(1000, "LynchEP", 2));

        }

        /// <summary>
        /// Adds a new colleague to the mediator and assigns the colleague a unique code.
        /// </summary>
        /// <param name="colleague">The new colleague to add</param>
        public void AddColleague(Colleague colleague)
        {
            colleagueCodes++;
            colleague.SetCollCode(colleagueCodes);
            colleagues.Add(colleague);

        }

        /// <summary>
        /// Buy and offer. If the offer is not found it will be added to the list. IF all quantities are sold the offer is removed.
        /// </summary>
        /// <param name="offerStock">The stock to buy</param>
        /// <param name="collCode">the colligue that is buying it</param>
        ///<returns>Returns a reponse if the operation was succesful</returns>
        public string BuyOffer(StockOffer offerStock, int collCode)
        {
            String stock = offerStock.StockSymbol;
            int shares = offerStock.StockShares;
            bool stockBought = false;
            String msg = "";
            foreach (StockOffer offer in stockSellOffers)
            {
                if (offer.StockSymbol == stock) //Check if the current stock is the same as the given
                {
                    if ((offer.StockShares == shares)) //If the stock ammount is = to the ammount to deduct, remove it from the list
                    {
                        stockSellOffers.Remove(offer);
                    }
                    else
                    {
                        //Deduct from the current stock
                        List<StockOffer> ofers = stockSellOffers.Where(x => x.StockSymbol == stock).ToList();

                        int tempShare = shares;
                        int maxToBuy = ofers.Sum(x => x.StockShares);
                        if (maxToBuy < tempShare)
                        {
                            return shares + " Shares of " + stock +
                             " were not able to be bought by colleague code " + collCode;
                        }
                        foreach (StockOffer of in ofers)
                        {
                            if (tempShare > of.StockShares)
                            {
                                tempShare -= of.StockShares;
                                stockSellOffers.Remove(of);
                            }
                            else if (tempShare == of.StockShares)
                            {
                                stockSellOffers.Remove(of);
                                break;
                            }
                            else if (tempShare < of.StockShares)
                            {
                                of.StockShares -= tempShare;
                                // tempShare -= of.StockShares;
                                break;
                            }
                        }
                    }
                    stockBought = true;
                    break;
                }
            }
            if (stockBought) //If not stock is bought, then that means it does not exist and is added to the market
            {
                msg = (shares + " share of " + stock +
                    " added to inventory");
                StockOffer newOffering = new StockOffer(shares, stock, collCode);
                stockBuyOffers.Add(newOffering);
            }

            OnStockChange();//Tells the main form the stock has changed
            SendNotifyToCollegues(); //Sends on stock change notification
            return msg;

        }


        /// <summary>
        /// Sale offer. If there is an offer such that matches the given offer. The offer is sold, if not it is added the list of offers
        /// </summary>
        /// <param name="offerStock">The offer ot be sold</param>
        /// <param name="collCode">The colleague selling the offer</param>
        ///<returns>Returns a reponse if the operation was succesful</returns>
        public string SaleOffer(StockOffer offerStock, int collCode)
        {
            String stock = offerStock.StockSymbol;
            int shares = offerStock.StockShares;
            
            string msg = "";
            foreach (StockOffer offer in stockBuyOffers)
            {
                if ((offer.StockSymbol == stock))//Check if the current stock is the same as the given
                {
                    offer.StockShares += shares;
                }

            }

            msg = (shares + " Shares of " + stock +
                        " sold by colleague " + collCode);
            StockOffer newOffering = new StockOffer(shares, stock, collCode);
            stockSellOffers.Add(newOffering);

            OnStockChange();//Tells the main form the stock has changed
            SendNotifyToCollegues(); //Sends on stock change notification
            return msg;


        }




        /// <summary>
        /// Returns an array of list of stock offers 
        /// [0] is stock sell offers
        /// [1] is stock buy offers
        /// </summary>
        /// <returns>Array with list of stock offers</returns>
        public List<StockOffer>[] GetStockOfferings()
        {
            List<StockOffer>[] offers = new List<StockOffer>[2];
            offers[0] = stockSellOffers;

            offers[1] = stockBuyOffers;

            return offers;
        }


        /// <summary>
        /// Sends a notification to all the colleagues. Used to update the interface of the users.
        /// </summary>
        private void SendNotifyToCollegues()
        {
            foreach (Colleague c in colleagues)
            {
                c.NotiftyChange();
            }
        }

        /// <summary>
        /// Gets the current registered colleagues to the mediator.
        /// </summary>
        /// <returns>Returns the colleague count</returns>
        public int GetColleagueCount()
        {
            return this.colleagues.Count;
        }
    }
}
