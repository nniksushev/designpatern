﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator_Pattern
{
    /// <summary>
    /// Interface for the mediator
    /// </summary>
    public interface IMediator
    {

        string SaleOffer(StockOffer offer, int collCode);
        string BuyOffer(StockOffer offer, int collCode);
        void AddColleague(Colleague colleague);
        List<StockOffer>[] GetStockOfferings();
        Action OnStockChange { get; set; }

        /// <summary>
        /// For testing perposes
        /// </summary>
        /// <returns>Returns the count of the colleagues</returns>
        int GetColleagueCount();
    }
}
