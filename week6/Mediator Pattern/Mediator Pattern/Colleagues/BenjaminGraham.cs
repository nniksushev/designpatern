﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator_Pattern
{
    /// <summary>
    /// Concrete Colleague
    /// </summary>
    public class BenjaminGraham : Colleague
    {
        public BenjaminGraham(IMediator newMediator) : base(newMediator, "Benjamin Graham")
        {
            Console.Out.WriteLine("Graham is considered the father of value investing, an investment approach he began teaching at Columbia Business School in 1928");
        }
    }
}
