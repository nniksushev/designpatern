﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator_Pattern
{
    /// <summary>
    /// Concrete Colleague
    /// </summary>
    public class PeterLynch : Colleague
    {
        public PeterLynch(IMediator newMediator) :base(newMediator, "Peter Lynch")
        {
            Console.Out.WriteLine("Peter Lynch Lynch averaged a 29.2% annual return, consistently more than doubling the S&P 500 market index and making it the best performing mutual fund in the world");
        }
    }
}
