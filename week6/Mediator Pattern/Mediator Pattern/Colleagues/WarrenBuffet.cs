﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator_Pattern
{
    /// <summary>
    /// Concrete Colleague
    /// </summary>
    public class WarrenBuffet : Colleague
    { 
        public WarrenBuffet(IMediator newMediator): base(newMediator, "Warren Buffet")
        {
            Console.Out.WriteLine("Warren Buffet is noted for his adherence to value investing and for his personal frugality despite his immense wealth");
        }
    }
}
