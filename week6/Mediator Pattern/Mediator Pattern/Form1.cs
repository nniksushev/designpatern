﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mediator_Pattern
{
    public partial class Form1 : Form
    {
        IMediator NYSE;
        Colleague WB;
        Colleague BG;
        Colleague PL;

        public Form1()
        {
            InitializeComponent();
           
            NYSE = new StockMediator();

            NYSE.OnStockChange = () => UpdateStockList();
            WB = new WarrenBuffet(NYSE);
            CollegueWindow win1 = new CollegueWindow(WB);
            win1.Show();
            BG = new BenjaminGraham(NYSE);
            CollegueWindow win2 = new CollegueWindow(BG);
            win2.Show();
            PL = new PeterLynch(NYSE);
            CollegueWindow win3 = new CollegueWindow(PL);
            win3.Show();
            UpdateStockList();
           

        }

    

        private void UpdateStockList()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            List<StockOffer>[] offers = NYSE.GetStockOfferings();
            List<StockOffer> sortedSells = Utility.SortList(offers[0]);
            List<StockOffer> sortedbuys = offers[1];

            foreach (StockOffer o in sortedSells)
            {
                listBox2.Items.Add("For Sale: " + o.ToString());
            }

            foreach (StockOffer o in sortedbuys)
            {
                listBox1.Items.Add("Bought by: " + o.ColleagueCode.ToString() +
                    ", Amount of " + o.ToString());
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
