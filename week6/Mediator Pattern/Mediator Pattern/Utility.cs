﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator_Pattern
{
    public class Utility
    {
        public static List<StockOffer> SortList(List<StockOffer> offers)
        {
            List<StockOffer> list = new List<StockOffer>();
            for (int i = 0; i < offers.Count; i++)
            {
                list.Add(offers[i]);
            }
            List<StockOffer> srted = new List<StockOffer>();
            foreach (StockOffer of in list)
            {
                if (srted.Find(x => x.StockSymbol == of.StockSymbol) == null)
                {

                    srted.Add(new StockOffer(of.StockShares, of.StockSymbol, of.ColleagueCode));
                }
            }
            for (int i = 0; i < srted.Count; i++)
            {
                StockOffer of = srted[i];
                if (of != null)
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        StockOffer other = list[j];
                        if (other != null)
                            if (other.StockSymbol == of.StockSymbol)
                            {
                                of.StockShares = of.StockShares + other.StockShares;
                                other = null;
                            }
                    }
            }
            //offers = list;
            return srted;
        }
    }
}
