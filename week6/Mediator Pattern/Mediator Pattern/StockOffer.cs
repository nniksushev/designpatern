﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator_Pattern
{
    /// <summary>
    /// Class holding the information for a stock offer, who sold it, ammount sold and the stock symbol.
    /// </summary>
    public class StockOffer
    {
        private int stockShares = 0;
        private String stockSymbol = "";
        private int colleagueCode = 0;

        /// <summary>
        /// Constructor for a stock
        /// </summary>
        /// <param name="numOfShares">The number of shares to be sold/bought</param>
        /// <param name="stock">The stock code of the stock</param>
        /// <param name="collCode">the colleague who sold it/bought</param>
        public StockOffer(int numOfShares, String stock, int collCode)
        {
            stockShares = numOfShares;
            stockSymbol = stock;
            colleagueCode = collCode;
        }

        /// <summary>
        /// Property that returns and sets the stock shares
        /// </summary>
        public int StockShares
        {
            get
            {
                return stockShares;
            }
            set
            {
                stockShares = value;
            }
        }
        /// <summary>
        /// Property to retrun the stock symbol
        /// </summary>
        public string StockSymbol
        {
            get
            {
                return stockSymbol;
            }
        }
        /// <summary>
        /// Property to ge the colleague code
        /// </summary>
        public int ColleagueCode
        {
            get
            {
                return colleagueCode;
            }
        }
        /// <summary>
        /// To string method which returns the Stock information in format : StockShares of StockSymbol 
        /// 5 of NASDAQ
        /// </summary>
        /// <returns>The stock in a string readable format</returns>
        public override string ToString()
        {
            return string.Format(StockShares.ToString() + " of " + StockSymbol);
        }

    }
}
