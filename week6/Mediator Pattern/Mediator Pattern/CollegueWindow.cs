﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mediator_Pattern
{
    public partial class CollegueWindow : Form
    {
        Colleague colleague; //The colleague of the window
        List<StockOffer> buyOffers; //The offers the user knows that are to be bought
        List<StockOffer> sellOffers; //The offers the user knows that are to be sold
        private StockOffer selectedOffer; //The current offer the user has selected

        /// <summary>
        /// Create a new Colleague window with the given colleague
        /// </summary>
        /// <param name="colleague">The colleague that will be using the window</param>
        public CollegueWindow(Colleague colleague)
        {

            this.colleague = colleague;
            this.colleague.RegisterToMediator();
            this.colleague.UpdateForm = () => GetOrdersAndUpdate(); //Used to assign the auto-update event when a new order has been sold/bought by someone
            InitializeComponent();
            GetOrdersAndUpdate();//Initial load of content
        }

        /// <summary>
        /// Updates the listbox with a specific list of stocks
        /// </summary>
        /// <param name="stock">The stock to use for update</param>
        /// <param name="listToUpdate">The listbox to update</param>
        //private void UpdateListBoxWithStock(List<StockOffer> stock, ListBox listToUpdate)
        //{
        //    listToUpdate.Items.Clear();
        //    foreach (StockOffer of in stock)
        //    {
        //        listToUpdate.Items.Add(of.ToString());
        //    }
        //}

        /// <summary>
        /// Sends a call to the mediator to get the newest stocks and update the user lists
        /// </summary>
        private void GetOrdersAndUpdate()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            List<StockOffer>[] offers = colleague.GetStock(); //call to mediator
            sellOffers = Utility.SortList(offers[0]);
            buyOffers = offers[1];
            //UpdateListBoxWithStock(sellOffers, listBox2);
            //UpdateListBoxWithStock(buyOffers, listBox1);
            

            foreach (StockOffer o in sellOffers)
            {
                listBox2.Items.Add("For Sale: " + o.ToString());
            }

            foreach (StockOffer o in buyOffers)
            {
                listBox1.Items.Add("Bought by: " + o.ColleagueCode.ToString() +
                    ", Amount of " + o.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StockOffer offer;
            //Buy offer for a specific amount by this current colleague
            try {
                int amount = Convert.ToInt32(numericUpDown1.Value);
               
                offer = new StockOffer(amount, selectedOffer.StockSymbol, this.colleague.ColleagueCode);              
                colleague.BuyOffer(offer);               
                GetOrdersAndUpdate();
                
            }
            catch (NullReferenceException)
            {
                Console.Out.WriteLine("choose a stock from the list for sale");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Send a sale offer for a specific stock and ammount by this current colleague
            string stockSymb = tbStockName.Text;
            if (stockSymb != "" && numericUpDown1.Value > 0)
            {
                StockOffer offer = new StockOffer((int)numericUpDown1.Value, stockSymb, colleague.ColleagueCode);
                colleague.SaleOffer(offer);
                GetOrdersAndUpdate();
            }
            else
            {
                Console.Out.WriteLine("Specify name and amount for selling offer");
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.ClearSelected();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.SelectedIndex > -1 && listBox2.SelectedIndex > -1)
                {
                    listBox1.ClearSelected(); //Clear the other selection

                }
                //Sets the current selected stock // in both to make sure the first click is registered
                int selected = listBox1.SelectedIndex;
                if (selected > -1)
                {
                    selectedOffer = buyOffers[selected];

                }
                else
                {
                    selected = listBox2.SelectedIndex;
                    if (selected > -1)
                    {
                        selectedOffer = sellOffers[selected];

                    }
                }
                numericUpDown1.Value = selectedOffer.StockShares;
            }
            catch (NullReferenceException) { }



        }
    }
}
