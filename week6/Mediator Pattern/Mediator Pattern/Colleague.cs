﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator_Pattern
{
    /// <summary>
    /// Abstract colleague class
    /// </summary>
    public abstract class Colleague
    {
        protected IMediator mediator; //The mediator reponsible for connections
        protected int colleagueCode; //The code to destinguish the colleague
        private Action updateForm; //The action for update of the form
        private string name;

        public string Name
        {
            get { return name; }
          
        }

        /// <summary>
        /// Property to get and set the colleagueCode ; protected set!
        /// </summary>
        public int ColleagueCode
        {
            get { return colleagueCode; }
            protected set { colleagueCode = value; }
        }

        
        /// <summary>
        /// Property to set and get the action for updating the form
        /// </summary>
        public Action UpdateForm
        {
            get { return updateForm; }
            set { updateForm = value; }
        }


        /// <summary>
        /// Constructor for colleague
        /// </summary>
        /// <param name="newMediator">The mediator to be used</param>
        public Colleague(IMediator newMediator, string name)
        {
            this.name = name;
            mediator = newMediator;
        }

        /// <summary>
        /// registers the current colleague to the mediator
        /// </summary>
        public void RegisterToMediator()
        {
            this.mediator.AddColleague(this);
        }

        /// <summary>
        /// Post a new sale offer
        /// </summary>
        /// <param name="offer">The offer to post</param>
        public void SaleOffer(StockOffer offer)
        {
            
           Console.Out.WriteLine(mediator.SaleOffer(offer, this.colleagueCode));
        }
        /// <summary>
        /// Buy an offer
        /// </summary>
        /// <param name="offer">buys a specific offer</param>
        public void BuyOffer(StockOffer offer)
        {
            Console.Out.WriteLine(mediator.BuyOffer(offer, this.colleagueCode));
        }
     
        /// <summary>
        /// Gets an array of List<StockOffer> with the stocks
        /// </summary>
        /// <returns></returns>
        public List<StockOffer>[] GetStock()
        {
            return mediator.GetStockOfferings();
        }

        /// <summary>
        /// Sets the current colleague code
        /// </summary>
        /// <param name="colleagueCode">The colleague code to change with</param>
        public void SetCollCode(int colleagueCode)
        {
            this.ColleagueCode = colleagueCode;
        }

        /// <summary>
        /// Virtual. Sends a notify to the update form method
        /// </summary>
        public virtual void NotiftyChange()
        {
            updateForm();
        }
    }
}
