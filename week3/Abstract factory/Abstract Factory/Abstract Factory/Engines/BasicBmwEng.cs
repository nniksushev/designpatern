﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Engines
{
    public class BasicBmwEng : Color
    {
        public BasicBmwEng()
        {
            name = "m33s1";
            horsePower = 200;
            type = "V6 2.5L";
            transmission = "Automatic/Manual";
            this.image = new System.Drawing.Bitmap("engine-pic/basic-bmw.jpg");
        }
    }
}
