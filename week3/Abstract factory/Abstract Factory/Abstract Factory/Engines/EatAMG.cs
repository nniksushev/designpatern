﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Engines
{
    public class EatAMG : Color
    {
        public EatAMG()
        {
            name = "m50b20";
            horsePower = 540;
            type = "V-12, 6L";
            transmission = "Automatic";
            this.image = new System.Drawing.Bitmap("engine-pic/eat-amg.jpg");
        }
    }
}
