﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Engines
{
    public class EatHonda : Color
    {
        public EatHonda()
        {
            name = "m44e29";
            horsePower = 660;
            type = "V8 6.2L";
            transmission = "Automatic/Manual";
            this.image = new System.Drawing.Bitmap("engine-pic/eat-honda.jpg");
        }
    }
}
