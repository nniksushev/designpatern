﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Engines
{
    public class MercAAA : Color
    {
        public MercAAA()
        {
            name = "merAAA";
            horsePower = 210;
            type = "2.5L";
            transmission = "Automatic";
            this.image = new System.Drawing.Bitmap("engine-pic/merc-aaa.jpg");
        }
    }
}
