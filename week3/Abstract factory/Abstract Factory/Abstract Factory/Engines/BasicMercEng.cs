﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Engines
{
    public class BasicMercEng : Color
    { 
        public BasicMercEng()
        {
            name = "MercB";
            horsePower = 330;
            type = "V8 4.4L";
            transmission = "Automatic";
            this.image = new System.Drawing.Bitmap("engine-pic/basic-merc.jpg");
        }
    }
}
