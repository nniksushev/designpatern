﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Engines
{
    public class B38 : Color
    {
        public B38()
        {
            name = "b38";
            horsePower = 330;
            type = "V8 4.4L";
            transmission = "Automatic";
            this.image = new System.Drawing.Bitmap("engine-pic/b38.jpg");
        }
    }
}
