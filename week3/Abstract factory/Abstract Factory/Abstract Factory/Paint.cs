﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory
{
    /// <summary>
    /// Class for the paint of the color
    /// </summary>
    public abstract class Paint
    {
        protected string color; // string of the color name

        /// <summary>
        /// To string returning the color name
        /// </summary>
        /// <returns>Returns color of car</returns>
        public override string ToString()
        {
            return color;
        }
    }
}
