﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Paints
{
    public class Orange : Paint
    {
        public Orange()
        {
            color = "orange";
        }
    }
}
