﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory
{
    /// <summary>
    /// Class with static method to generate a random car
    /// </summary>
    public class RandomCarGenerator
    {
        /// <summary>
        /// Cretes a random car from types "Sedan" , "Coupe" or" Hatchback"
        /// </summary>
        /// <param name="carDealership">The current dealership that wants the car</param>
        /// <returns>Returns the car</returns>
        internal static Car GenerateCar(CarDealership carDealership)
        {
            Random r = new Random();

            int index = r.Next(0, 3);
            CarTypes type = (CarTypes)index;//index of car types to creat a type of car
            return carDealership.OrderCar(type);
        }

    }
}
