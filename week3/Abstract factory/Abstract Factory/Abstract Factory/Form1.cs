﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Abstract_Factory
{
    public partial class Form1 : Form
    {
        CarDealership carDealership; // the current dealership
        private CarDetails detailView;
        int extraCount = 0;
        CarTypes carType;
        TypeOfDealership typeOfDealership; // the type of the current dealership
        Thread generator;
        bool firstRun = true;

        public Form1()
        {
            InitializeComponent();

            //Initialize dealership  and dealership view
            typeOfDealership = TypeOfDealership.BMW;
            carDealership = new CarDealership(typeOfDealership);
            pictureBox1.Image = new Bitmap("logos/BMW.png");
            this.Text = typeOfDealership.ToString() + " Dealership";


            //random generator for car getting 
            generator = new Thread(new ThreadStart(RandomCarGenerate)); ;
            generator.Start();

            //Init listview headers
            listView1.Columns.Add("Model");
            listView1.Columns.Add("Color");
            listView1.Columns.Add("engine");
            listView1.View = View.Details;
            SizeLastColumn(listView1);

        }

        /// <summary>
        /// Resize last column size
        /// </summary>
        /// <param name="lv">The listview to resize</param>
        private void SizeLastColumn(ListView lv)
        {
            lv.Columns[lv.Columns.Count - 1].Width = -2;
        }

        /// <summary>
        /// Adds the car values to the listview
        /// </summary>
        /// <param name="car">The car to be added</param>
        private void FillCarToView(Car car)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                item.Font = new Font(item.Font, FontStyle.Regular); // makes the old values to normal

            }

            ListViewItem lvi = new ListViewItem();
            lvi.Text = car.Name;
            lvi.Font = new Font(lvi.Font, FontStyle.Bold); // makes the new value to bold
            ListViewItem.ListViewSubItem[] items = car.GetListViewItems(); // gets the list view items
            if (items.Length - 2 > extraCount) // if there is need to add a new column
            {
                for (int i = 0; i < items.Length - extraCount - 2; i++) // how many to add
                {
                    this.Invoke(new Action(() => listView1.Columns.Add("Extras")));
                }
                extraCount = items.Length - 2;
            }
            foreach (ListViewItem.ListViewSubItem item in items) // adds the sub items to the listview
            {

                lvi.SubItems.Add(item);
            }

            this.Invoke(new Action(() => listView1.Items.Add(lvi)));
            this.Invoke(new Action(() =>
            {
                //Resize based on content
                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            ));


        }

        /// <summary>
        /// Random generator method
        /// </summary>
        private void RandomCarGenerate()
        {
            while (true)
            {
                Thread.Sleep(3000);
                Car car = RandomCarGenerator.GenerateCar(carDealership); //generates a random car
                this.Invoke(new Action(() => FillCarToView(car)));//fills the car to the listview
            }
        }

        private void btnOrderCar_Click(object sender, EventArgs e)
        {
            Car orderedCar;

            orderedCar = carDealership.OrderCar(carType); //orders a new car 
            this.Invoke(new Action(() => FillCarToView(orderedCar)));

        }

        private void rbSedan_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                carType = CarTypes.SEDAN;
            }
        }

        private void rbCoupe_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                carType = CarTypes.COUPE;
            }
        }

        private void rbHatchbag_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                carType = CarTypes.HATCHBAG;
            }
        }

        private void cbDealership_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (!firstRun) //to not show the message if the program just started
                if (MessageBox.Show("Are you sure? This will mean all cars will be removed from your store!",
                    "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes) // if the user wants to change dealership all previous cars will be removed and start fresh
                {
                    TypeOfDealership newDealership;
                    switch ((sender as ComboBox).Text) // initializze the new dealership
                    {
                        case "BMW":
                            newDealership = TypeOfDealership.BMW;
                            pictureBox1.Image = new Bitmap("logos/BMW.png");
                            break;

                        case "Mercedes":
                            newDealership = TypeOfDealership.Mercedes;
                            pictureBox1.Image = new Bitmap("logos/mercedes.png");
                            break;
                        default:
                            newDealership = TypeOfDealership.BMW;
                            pictureBox1.Image = new Bitmap("logos/BMW.png");
                            break;
                    }
                    if (newDealership != typeOfDealership) // if the old is the same as the current, do nothing
                    {
                        typeOfDealership = newDealership;
                        carDealership = new CarDealership(typeOfDealership);
                        listView1.Items.Clear();
                        this.Text = dealershipType.Text = typeOfDealership.ToString() + " Dealership";


                    }
                }
            firstRun = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbDealership.SelectedIndex = 0;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            generator.Abort(); // thread safe close random generator
        }

        private void butInspect_Click(object sender, EventArgs e)
        {
            if (detailView == null)
            {

            }
            else
            {
                detailView.Close(); // makes sure the detail view is closed before opening another one
            }
            if (listView1.SelectedItems.Count != 0) // makes sure there is a selected index from the list view
            {
                int index = listView1.SelectedIndices[0];// gets the first selected ( it is possible to select multiple )
                detailView = new CarDetails(carDealership.GetCar(index));
                detailView.Show();
            }


        }



        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listView1.SelectedIndices.Count != 0) // interactive is selected -> allow to inspect
            {
                butInspect.Enabled = true;
            }
            else
            {
                butInspect.Enabled = false;
            }
        }
    }
}
