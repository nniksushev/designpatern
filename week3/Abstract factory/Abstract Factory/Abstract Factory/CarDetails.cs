﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Abstract_Factory
{
    public partial class CarDetails : Form
    {
        private Car car;
        List<IImagable> images;
        int currentImage = 0;
        bool isOnTestDrive;
        bool switchToInspect;
        Thread speedThread; //to keep track of current speed.
        /// <summary>
        /// Instanciate a new form with the car that was selected
        /// </summary>
        /// <param name="car">The car to be viewed</param>
        public CarDetails(Car car)
        {
            isOnTestDrive = false;
            images = new List<IImagable>();
            InitializeComponent();
            this.car = car;
            this.pbImage.Image = car.Image;

            //populates the images in the combo box
            this.images = car.ImagesToShow();
            images.Add(car);
        }

        private void CarDetails_Load(object sender, EventArgs e)
        {
            labCurrentView.Text += car.Name;

            foreach (String item in car.ListOfComponentsToShow())
            {
                cbPartToView.Items.Add(item);
            }
            cbPartToView.SelectedIndex = 0;
            cbPartToView.Items.Add(car.Name);
        }

        private void butPrevious_Click(object sender, EventArgs e)
        {
            // moves one image back or loop to end
            --currentImage;
            if (currentImage < 0)
            {
                currentImage = images.Count - 1;
            }
            cbPartToView.SelectedIndex = currentImage;
        }

        private void butNext_Click(object sender, EventArgs e)
        {
            //changes to next image or loop to start
            ++currentImage;
            if (currentImage >= images.Count)
            {
                currentImage = 0;
            }
            cbPartToView.SelectedIndex = currentImage;
        }

        private void cbPartToView_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentImage = (sender as ComboBox).SelectedIndex;
            Bitmap image = images[currentImage].Image;
            this.pbImage.Image = image;
        }

        private void butTestDrive_Click(object sender, EventArgs e)
        {
            //if the car is on test drive, stop it, if not, start a test drive
            if (!isOnTestDrive)
            {
                // loads values to form for car test drive
                car.PrepareForTestDrive(); // loads the people in the car
                this.pbImage.Image = car.Image;
                //Starts the test run
                //and init form button and label texts
                TestRun();


                butTestDrive.Text = "Stop test drive.";
                labCurrentView.Text = "Test driving car: " + car.Name;
            }
            else
            {
                car.StopTestDrive();
                switchToInspect = true;
                if (labSpeed.Text == "Speed: 0" )
                {
                    SwitchToInspect();
                }
               
            }
        }

        private void SwitchToInspect()
        {
           
            car.UnloadCarWithPassengers();
            this.cbPartToView.SelectedIndex = this.cbPartToView.Items.Count - 1; // returns the view to default car inspection 
            this.pbImage.Image = car.Image;
            //Stops the test run
            //and init form button and label texts
            TestRun();
            butTestDrive.Text = "Take for a test drive.";
            labCurrentView.Text = "Currently viewing details about:  " + car.Name;
        }

        private void TestRun()
        {
            isOnTestDrive = !isOnTestDrive; // if true make false, of false make true

            //Form inspection = true, make form labels and buttons visible for inspection
            //else show test drive
            butNext.Visible = !isOnTestDrive;
            butPrevious.Visible = !isOnTestDrive;
            butNext.Enabled = !isOnTestDrive;

            butPrevious.Enabled = !isOnTestDrive;
            label1.Visible = !isOnTestDrive;
            cbPartToView.Visible = !isOnTestDrive;
            cbPartToView.Enabled = !isOnTestDrive;

            //Form inspection = false, make form labels and buttons visible for car test drive
            //else show inspection
            butEngine.Enabled = isOnTestDrive;
            butEngine.Visible = isOnTestDrive;

            labSpeed.Visible = isOnTestDrive;
            butTakeForSpin.Visible = isOnTestDrive;
            butTakeForSpin.Enabled = !isOnTestDrive; // engine is not started yet
        }

        private void butEngine_Click(object sender, EventArgs e)
        {
            //Start the engine
            if (!car.IsStarted)
            {
                car.StartEngine();

                butTakeForSpin.Enabled = car.IsStarted;
            }
            //Stop the engine
            else
            {
                car.StopEngine();
                switchToInspect = false;
                butTakeForSpin.Enabled = false;
            }
            //Change the text of the button for engine
            butEngine.Text = car.IsStarted ? "Stop engine." : "Start engine";
        }



        private void butTakeForSpin_Click(object sender, EventArgs e)
        {
            //Make the car move
            car.TestDrive();
            if (speedThread != null) speedThread.Abort(); // Thread safe closing of form

            speedThread = new Thread(new ThreadStart(FollowCarSpeed));
            speedThread.Start();
            labSpeed.Visible = true;
            butTakeForSpin.Enabled = false;

        }

        /// <summary>
        /// Thread method to keep track of the car speed using a thread
        /// </summary>
        private void FollowCarSpeed()
        {
            double speed = 0;
            while (car.IsStarted) //while the car's engine is started
            {
                while (speed != car.Speed) // second while to keep chaning the speed
                {
                    Thread.Sleep(100);


                    if (speed > car.Speed)
                    {
                        speed--;
                    }
                    else
                    {
                        speed++;
                    }
                    this.Invoke(new Action(() => labSpeed.Text = "Speed: " + speed.ToString()));

                }

            }
            if (isOnTestDrive && switchToInspect) SwitchToInspect();
        }

        private void CarDetails_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (speedThread != null) // thread safe close of threads
                speedThread.Abort();
            car.AbortThread();// thread safe close of car thread for speed 
        }
    }
}
