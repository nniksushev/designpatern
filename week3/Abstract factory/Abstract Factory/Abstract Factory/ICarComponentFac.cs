﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory
{
    /// <summary>
    /// Interface for the component factories
    /// </summary>
    public interface ICarComponentFac
    {

        //Properties 
        string Type { get; } 
        string TypeOfCar { get; set; }

        //Methods
        Color CreateEngine();
        Paint CreatePaint();
        List<Extra> CreateExtras();
    }
}
