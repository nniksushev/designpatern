﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory
{
    public abstract class Extra : IImagable
    {
        protected string name;
        protected int price;
        protected Bitmap image;

        /// <summary>
        /// Image to be viewed for the extra
        /// </summary>
        public Bitmap Image
        {
            get
            {
                return this.image;
            }
            protected set
            {
                this.image = value;
            }
        }

        /// <summary>
        /// To string to view the extra with its price
        /// </summary>
        /// <returns>Name $ price</returns>
        public override string ToString()
        {
            return name
                    + " $" + price.ToString();
        }

        /// <summary>
        /// Gets the name of the extra
        /// </summary>
        public String Name
        {
            get { return this.name; }
        }
    }
}
