﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Extras
{
    public class Heating : Extra
    {
        public Heating()
        {
            name = "Extra heating";
            price = 4400;
            this.image = new System.Drawing.Bitmap("extras-pic/heating.jpg");
        }
    }
}
