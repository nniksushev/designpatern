﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Extras
{
    public class Comfort : Extra
    {
        public Comfort()
        {
            name = "extra comfort";
            price = 5500;
            this.image = new System.Drawing.Bitmap("extras-pic/extra-comfort.jpg");
        }
    }
}
