﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Extras
{
    public class Leather : Extra
    {
        public Leather()
        {
            name = "Leather interior";
            price = 4300;
            this.image = new System.Drawing.Bitmap("extras-pic/leather.jpg");
        }
    }
}
