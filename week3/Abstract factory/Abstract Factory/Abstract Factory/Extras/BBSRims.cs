﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Extras
{
    public class BBSRims : Extra
    {
        public BBSRims()
        {
            name = "BBS Rims";
            price = 5200;
            this.image = new System.Drawing.Bitmap("extras-pic/bbs-rims.jpg");
        }
    }
}
