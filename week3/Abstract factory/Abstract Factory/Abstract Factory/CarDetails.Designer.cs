﻿namespace Abstract_Factory
{
    partial class CarDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbImage = new System.Windows.Forms.PictureBox();
            this.cbPartToView = new System.Windows.Forms.ComboBox();
            this.labCurrentView = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.butPrevious = new System.Windows.Forms.Button();
            this.butNext = new System.Windows.Forms.Button();
            this.butTestDrive = new System.Windows.Forms.Button();
            this.butEngine = new System.Windows.Forms.Button();
            this.butTakeForSpin = new System.Windows.Forms.Button();
            this.labSpeed = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pbImage
            // 
            this.pbImage.Location = new System.Drawing.Point(48, 73);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(295, 239);
            this.pbImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImage.TabIndex = 0;
            this.pbImage.TabStop = false;
            // 
            // cbPartToView
            // 
            this.cbPartToView.FormattingEnabled = true;
            this.cbPartToView.Location = new System.Drawing.Point(134, 366);
            this.cbPartToView.Name = "cbPartToView";
            this.cbPartToView.Size = new System.Drawing.Size(121, 21);
            this.cbPartToView.TabIndex = 1;
            this.cbPartToView.SelectedIndexChanged += new System.EventHandler(this.cbPartToView_SelectedIndexChanged);
            // 
            // labCurrentView
            // 
            this.labCurrentView.AutoSize = true;
            this.labCurrentView.Location = new System.Drawing.Point(45, 26);
            this.labCurrentView.Name = "labCurrentView";
            this.labCurrentView.Size = new System.Drawing.Size(156, 13);
            this.labCurrentView.TabIndex = 2;
            this.labCurrentView.Text = "Currently viewing details about: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(148, 340);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select part to view";
            // 
            // butPrevious
            // 
            this.butPrevious.Location = new System.Drawing.Point(48, 318);
            this.butPrevious.Name = "butPrevious";
            this.butPrevious.Size = new System.Drawing.Size(75, 23);
            this.butPrevious.TabIndex = 4;
            this.butPrevious.Text = "< previous ";
            this.butPrevious.UseVisualStyleBackColor = true;
            this.butPrevious.Click += new System.EventHandler(this.butPrevious_Click);
            // 
            // butNext
            // 
            this.butNext.Location = new System.Drawing.Point(268, 318);
            this.butNext.Name = "butNext";
            this.butNext.Size = new System.Drawing.Size(75, 23);
            this.butNext.TabIndex = 5;
            this.butNext.Text = "next >";
            this.butNext.UseVisualStyleBackColor = true;
            this.butNext.Click += new System.EventHandler(this.butNext_Click);
            // 
            // butTestDrive
            // 
            this.butTestDrive.Location = new System.Drawing.Point(365, 5);
            this.butTestDrive.Name = "butTestDrive";
            this.butTestDrive.Size = new System.Drawing.Size(66, 55);
            this.butTestDrive.TabIndex = 6;
            this.butTestDrive.Text = "Take for a test drive.";
            this.butTestDrive.UseVisualStyleBackColor = true;
            this.butTestDrive.Click += new System.EventHandler(this.butTestDrive_Click);
            // 
            // butEngine
            // 
            this.butEngine.Enabled = false;
            this.butEngine.Location = new System.Drawing.Point(48, 364);
            this.butEngine.Name = "butEngine";
            this.butEngine.Size = new System.Drawing.Size(75, 23);
            this.butEngine.TabIndex = 7;
            this.butEngine.Text = "Start Engine";
            this.butEngine.UseVisualStyleBackColor = true;
            this.butEngine.Visible = false;
            this.butEngine.Click += new System.EventHandler(this.butEngine_Click);
            // 
            // butTakeForSpin
            // 
            this.butTakeForSpin.Enabled = false;
            this.butTakeForSpin.Location = new System.Drawing.Point(268, 358);
            this.butTakeForSpin.Name = "butTakeForSpin";
            this.butTakeForSpin.Size = new System.Drawing.Size(75, 35);
            this.butTakeForSpin.TabIndex = 8;
            this.butTakeForSpin.Text = "Take for a spin";
            this.butTakeForSpin.UseVisualStyleBackColor = true;
            this.butTakeForSpin.Visible = false;
            this.butTakeForSpin.Click += new System.EventHandler(this.butTakeForSpin_Click);
            // 
            // labSpeed
            // 
            this.labSpeed.AutoSize = true;
            this.labSpeed.Location = new System.Drawing.Point(131, 350);
            this.labSpeed.Name = "labSpeed";
            this.labSpeed.Size = new System.Drawing.Size(50, 13);
            this.labSpeed.TabIndex = 9;
            this.labSpeed.Text = "Speed: 0";
            this.labSpeed.Visible = false;
            // 
            // CarDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 399);
            this.Controls.Add(this.labSpeed);
            this.Controls.Add(this.butTakeForSpin);
            this.Controls.Add(this.butEngine);
            this.Controls.Add(this.butTestDrive);
            this.Controls.Add(this.butNext);
            this.Controls.Add(this.butPrevious);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labCurrentView);
            this.Controls.Add(this.cbPartToView);
            this.Controls.Add(this.pbImage);
            this.Name = "CarDetails";
            this.Text = "Car Details";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CarDetails_FormClosed);
            this.Load += new System.EventHandler(this.CarDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbImage;
        private System.Windows.Forms.ComboBox cbPartToView;
        private System.Windows.Forms.Label labCurrentView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butPrevious;
        private System.Windows.Forms.Button butNext;
        private System.Windows.Forms.Button butTestDrive;
        private System.Windows.Forms.Button butEngine;
        private System.Windows.Forms.Button butTakeForSpin;
        private System.Windows.Forms.Label labSpeed;
    }
}