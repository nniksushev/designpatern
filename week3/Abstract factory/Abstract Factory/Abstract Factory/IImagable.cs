﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory
{
    /// <summary>
    /// Interface to make sure components have an image to display
    /// </summary>
    public interface IImagable
    {
        Bitmap Image { get;}
    }
}
