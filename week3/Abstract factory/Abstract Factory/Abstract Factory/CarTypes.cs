﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory
{
    /// <summary>
    /// Car types with indexes
    /// SEDAN = 0,
    /// COUPE = 1,
    /// HATCHBAG = 2
    /// </summary>
    public enum CarTypes
    {
        SEDAN = 0,
        COUPE = 1,
        HATCHBAG = 2
    }
}
