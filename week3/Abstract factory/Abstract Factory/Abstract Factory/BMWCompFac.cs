﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abstract_Factory.Engines;
using Abstract_Factory.Extras;
using Abstract_Factory.Paints;

namespace Abstract_Factory
{
    public class BMWCompFac : ICarComponentFac
    {
        String typeOfCar;
        Extra[] possibleExtras = new Extra[] { new Comfort(), new Leather() }; //list of possible extras for a bmw

        /// <summary>
        /// Constructor for BMW Component factory
        /// </summary>
        public BMWCompFac()
        {
            
        }

        /// <summary>
        /// Returns BMW, the type of the factory
        /// </summary>
        public string Type
        {
            get
            {
                return "BMW";
            }
        }

        /// <summary>
        /// Sets and gets the value of the car to create
        /// </summary>
        public string TypeOfCar
        {
            get
            {
               return typeOfCar;
            }

            set
            {
                typeOfCar = value;
            }
        }

        /// <summary>
        /// Creates an engine dependingo n the type of car to create
        /// </summary>
        /// <returns>Returns an engine for the specific model</returns>
        Color ICarComponentFac.CreateEngine()
        {
            if(typeOfCar == "Sedan")
            {
                return new EatHonda();//YUM
            }
            else if (typeOfCar == "Coupe")
            {
                return new EatAMG();
            }
            else
            {
                return new BasicBmwEng(); 
            }       
        }

        /// <summary>
        /// Creates random extras for the car
        /// </summary>
        /// <returns>A list of extras</returns>
        List<Extra> ICarComponentFac.CreateExtras()
        {
            Random r = new Random();
            int extasCount = r.Next(0, possibleExtras.Length + 1); //random from 0 to 2 extras
            List<Extra> extras = new List<Extra>();
            for (int i = 0; i < extasCount; i++)
            {
                //Gets a random from the possible and checks if it is already there if it is not adds it else, does not add it
                int extraIndex = r.Next(0, possibleExtras.Length);
                Extra ex = possibleExtras[extraIndex];
                if (!extras.Contains(ex))
                    extras.Add(ex);
            }

            return extras; 
        }

        /// <summary>
        /// Creates the paint of the car
        /// </summary>
        /// <returns>Returns the car paint object</returns>
        Paint ICarComponentFac.CreatePaint()
        {
            return new Silver();
        }
    }
}
