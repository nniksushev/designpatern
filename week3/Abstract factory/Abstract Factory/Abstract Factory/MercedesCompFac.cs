﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abstract_Factory.Engines;
using Abstract_Factory.Extras;
using Abstract_Factory.Paints;

namespace Abstract_Factory
{
    /// <summary>
    /// Merdeces component factory implmenting ICarComponentFac
    /// </summary>
    public class MercedesCompFac : ICarComponentFac
    {

        String typeOfCar;
        Extra[] possibleExtras = new Extra[] { new BBSRims(),  new Heating()}; //Possible extras list

        /// <summary>
        /// Constructor for component Factory
        /// </summary>
        public MercedesCompFac()
        {
           
        }

        /// <summary>
        /// Type of the factory, gets "Mercedes"
        /// </summary>
        public string Type
        {
            get
            {
                return "Mercedes";
            }
        }

        /// <summary>
        /// Gets and Sets The type of the car to currently create
        /// </summary>
        public string TypeOfCar
        {
            get
            {
                return typeOfCar;
            }

            set
            {
                typeOfCar = value;
            }
        }

        /// <summary>
        /// Creates an engine for the type of the car
        /// </summary>
        /// <returns>Returns an engine depnding on the car type</returns>

        Color ICarComponentFac.CreateEngine()
        {
            if (typeOfCar == "Sedan")
            {
                return new B38();
            }
            else if (typeOfCar == "Coupe")
            {
                return new MercAAA();
            }
            else
            {
                return new BasicMercEng();
            }
        }

        /// <summary>
        /// Creates the extras of the car
        /// </summary>
        /// <returns>List of extras 0, to 2 extras</returns>

        List<Extra> ICarComponentFac.CreateExtras()
        {
            Random r = new Random();
            int extasCount = r.Next(0, possibleExtras.Length + 1); // 0.. 2 extras
            List<Extra> extras = new List<Extra>();
            for (int i = 0; i < extasCount; i++)
            {
                //If extra is there, do not add it, if not, do 
                Extra ex = possibleExtras[r.Next(0, possibleExtras.Length)];
                if (!extras.Contains(ex))
                    extras.Add(ex);
            }

            return extras;
        }

        /// <summary>
        /// Creates the paint of the car
        /// </summary>
        /// <returns>Returns Black as the paint</returns>
        Paint ICarComponentFac.CreatePaint()
        {
            return new Black();
        }
    }
}
