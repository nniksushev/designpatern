﻿using Abstract_Factory.Cars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory
{
    /// <summary>
    /// Class to create a car dealership 
    /// </summary>
    public class CarDealership
    {
        List<Car> sellingCars; // the car list of the current dealership 
        ICarComponentFac factory; // the factory that will be the supplier 

        /// <summary>
        /// Constructor for new dealership depending on the type of dealership
        /// </summary>
        /// <param name="dealership">The type of the dealership to create</param>
        public CarDealership(TypeOfDealership dealership)
        {
            sellingCars = new List<Car>();
            switch (dealership)
            {
                case TypeOfDealership.BMW:
                    factory = new BMWCompFac(); //makes the supplier from BMW
                    break;
                case TypeOfDealership.Mercedes: //makes the supplier from Mercedes
                    factory = new MercedesCompFac();
                    break;
                case TypeOfDealership.Citroen:
                    throw new NotImplementedException(); //Not implmented in this version, for future development

                case TypeOfDealership.VW:
                    throw new NotImplementedException();//Not implmented in this version, for future development
                default:
                    factory = new BMWCompFac();//default if invalid type was specified

                    break;
            }

        }

        /// <summary>
        /// Dealership orders a car from the factory by CarTypes 'Coupe' or  'Hatchback' or  'Sedan'
        /// </summary>
        /// <param name="type">Type of car 'Coupe' or  'Hatchback' or  'Sedan'</param>
        /// <returns>A new fresh car from the factory, mind smell of a new  car</returns>
        public Car OrderCar(CarTypes type)
        {

            Car car = CreateCar(type);
            return car;
        }

        /// <summary>
        /// Based on car type creates a new car with a name
        /// </summary>
        /// <param name="type">Type of the car</param>
        /// <returns>A new car</returns>
        protected Car CreateCar(CarTypes type)
        {
            Car car = null;
            //creates a new car from the factory based on the type
            if (type == CarTypes.SEDAN)
            {
                car = new Sedan(factory);
            }
            else if (type == CarTypes.COUPE)
            {
                car = new Coupe(factory);
            }
            else if (type == CarTypes.HATCHBAG)
            {
                car = new Hatchbag(factory);
            }
            car.Construct(); // constructs the car with engine color and extras
            sellingCars.Add(car); //adds the car to the list
            return car;
        }

        /// <summary>
        /// Returns a specific car from the list of the dealership
        /// </summary>
        /// <param name="v">The index of the car to find</param>
        /// <returns>Returns a car from the list if found</returns>
        internal Car GetCar(int v)
        {
            if (v > sellingCars.Count - 1) return null;
            if (v < 0) return null;
            if (sellingCars.Count == 0) return null;
            return sellingCars[v];
        }
    }
}

/// <summary>
/// Type of car dealerships :  BMW, Mercedes, Citroen, VW
/// </summary>
public enum TypeOfDealership
{
    BMW, Mercedes, Citroen, VW
}
