﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory
{
    /// <summary>
    /// Class engine that implements IImagable
    /// </summary>
    public abstract class Color : IImagable
    {
        protected string name;
        protected int horsePower;
        protected string type; // the type of the engine by manufacture model
        protected string transmission; // manual or automatic or both
        protected Bitmap image;// image resource
        protected bool engineIsStarted; // for Test Drive 


        /// <summary>
        /// Gets and sets the engine is started 
        /// </summary>
        public bool EngineIsStarted
        {
            get { return engineIsStarted; }
            private set { engineIsStarted = value; }
        }


        /// <summary>
        /// Gets and sets the image of the engine
        /// </summary>
        public Bitmap Image
        {
            get
            {
                return this.image;
            }
           protected set
            {
                this.image = value;
            }
        }

        /// <summary>
        /// To string to view the engine in a more better way
        /// </summary>
        /// <returns>String values of the name type horse power and transmittion</returns>
        public override string ToString()
        {
            return name
                    + " - " + type
                    + " (" + horsePower.ToString() + ") with "
                    + transmission + " transmition";
        }

        /// <summary>
        /// Starts the engine of the car
        /// </summary>
        /// <returns>The state of the engine</returns>

        public bool StartEngine()
        {
            this.EngineIsStarted = true;
            return this.EngineIsStarted;
        }

        /// <summary>
        /// Stops the engine
        /// </summary>
        /// <returns>The state of the engine</returns>

        public bool StopEngine()
        {
            this.EngineIsStarted = false;
            return this.EngineIsStarted;
        }

        /// <summary>
        /// Returns a value of the speed by the horse power of the car
        /// </summary>
        /// <returns>Double with car speed</returns>
        public double GiveSpeed()
        {
            return (horsePower / 100 * 3 + 1) % 10;
        }
    }
}
