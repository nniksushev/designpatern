﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Cars
{
    public class Sedan : Car
    {

        public Sedan(ICarComponentFac fact) : base(fact)
        {
            factory.TypeOfCar = "Sedan";
            this.Name = "Sedan";
            switch (fact.Type)
            {
                case "BMW":
                    this.emptyCarImage = new System.Drawing.Bitmap("cars-pic/bmw-sedan.jpg");
                    this.fullCarImage = new System.Drawing.Bitmap("cars-pic/bmw-sedan-full.jpg");
                    break;
                case "Mercedes":
                    this.emptyCarImage = new System.Drawing.Bitmap("cars-pic/mercedes-sedan.jpg");
                    this.fullCarImage = new System.Drawing.Bitmap("cars-pic/mercedes-sedan-full.jpg");
                    break;
                default:
                    break;
            }
            this.image = emptyCarImage;

        }


    }
}
