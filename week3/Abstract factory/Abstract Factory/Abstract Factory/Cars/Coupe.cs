﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Cars
{
   public class Coupe : Car
    {
        public Coupe(ICarComponentFac fact) : base(fact)
        {
            factory.TypeOfCar = "Coupe";
            this.Name = "Coupe";
            switch (fact.Type)
            {
                case "BMW":
                    this.emptyCarImage = new System.Drawing.Bitmap("cars-pic/bmw-coupe.jpg");
                    this.fullCarImage = new System.Drawing.Bitmap("cars-pic/bmw-coupe-full.jpg");

                    break;
                case "Mercedes":
                    this.emptyCarImage = new System.Drawing.Bitmap("cars-pic/mercedes-coupe.jpg");
                    this.fullCarImage = new System.Drawing.Bitmap("cars-pic/mercedes-coupe-full.jpg");
                    break;
                default:
                    break;
            }
            this.image = emptyCarImage;
        }
    }
}
