﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Factory.Cars
{
    public class Hatchbag : Car
    {
        public Hatchbag(ICarComponentFac fact) : base(fact)
        {
            fact.TypeOfCar = "Hatchbag";
            this.Name = "Hatchbag";
            switch (fact.Type)
            {
                case "BMW":
                    this.emptyCarImage = new System.Drawing.Bitmap("cars-pic/bmw-hatchback.jpg");
                    this.fullCarImage = new System.Drawing.Bitmap("cars-pic/bmw-hatchback-full.jpg");
                    break;
                case "Mercedes":
                    this.emptyCarImage = new System.Drawing.Bitmap("cars-pic/mercedes-hatchback.jpg");
                    this.fullCarImage = new System.Drawing.Bitmap("cars-pic/mercedes-hatchback-full.jpg");
                    break;
                default:
                    break;
            }
            this.image = emptyCarImage;
        }
    }
}
