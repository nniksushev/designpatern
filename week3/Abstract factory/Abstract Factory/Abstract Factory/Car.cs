﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Abstract_Factory
{
    /// <summary>
    /// Abstract car class which implements IImagable
    /// </summary>
    public abstract class Car : IImagable
    {
        protected String name;
        protected Paint color;
        protected Color engine;
        protected List<Extra> extras;
        protected ICarComponentFac factory;

        //Images of the car
        protected Bitmap image; //current image
        protected Bitmap emptyCarImage; // default image
        protected Bitmap fullCarImage; // image with people

        private bool needToStop = false; //used for Test drive
        protected double maxSpeed = 200; //max speed limit

        protected double speed; // current speed
        Thread speedChanger; // thread for speed change
        /// <summary>
        /// Gets the current speed
        /// </summary>
        public double Speed
        {
            get { return speed; }

        }


        /// <summary>
        /// Constructor for car, Initializes the car to be created by specific factory
        /// </summary>
        /// <param name="_factory">The factory to create the car</param>
        protected Car(ICarComponentFac _factory)
        {
            factory = _factory;
        }

        /// <summary>
        /// Gets and sets the name of the Car
        /// </summary>
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Gets the current image
        /// </summary>
        public Bitmap Image
        {
            get
            {
                return this.image;
            }


        }

        /// <summary>
        /// Gets and sets the isStarted of the car , used for test drive to check if the car is running
        /// </summary>
        public bool IsStarted
        {
            get;
            protected set;
        }

        /// <summary>
        /// Tells the factory to construct the car and give it it's parts
        /// </summary>
        public virtual void Construct()
        {
            engine = factory.CreateEngine();
            color = factory.CreatePaint();
            extras = factory.CreateExtras();
        }


        /// <summary>
        /// To string method to make car easy to read
        /// </summary>
        /// <returns>Returns a string easy to read for the car components</returns>
        public override string ToString()
        {
            string listOfextras = "";
            foreach (Extra extra in extras)
            {
                listOfextras = listOfextras + " ," + extra;
            }
            return name + " "
                    + color.ToString()
                    + " " + engine.ToString()
                    + " " + listOfextras;

        }

        /// <summary>
        /// Creates a list of strings to visualize as headers for the listview
        /// </summary>
        /// <returns>A list of strings with component headers</returns>
        public List<String> ListOfComponentsToShow()
        {
            List<String> comp = new List<string>();

            comp.Add("Engine");
            for (int i = 0; i < extras.Count; i++)
            {
                comp.Add("Extra" + (i + 1) + " - " + extras[i].Name);
            }
            return comp;
        }

        /// <summary>
        /// Creates a list of images from all components
        /// </summary>
        /// <returns>An List of Bitmaps from all components of the car</returns>
        public List<IImagable> ImagesToShow()
        {
            List<IImagable> images = new List<IImagable>();
            images.Add(engine);
            foreach (Extra ex in extras)
            {
                images.Add(ex);
            }
            return images;

        }

        /// <summary>
        /// Initializes the current image to the full of people image
        /// </summary>
        internal void PrepareForTestDrive()
        {
            LoadCarWithPassengers();
        }

        /// <summary>
        /// Starts a thread to change the speed at random.
        /// </summary>
        internal void TestDrive()
        {
            if (speedChanger != null) speedChanger.Abort();
            speedChanger = new Thread(new ThreadStart(ChangeSpeed));
            speedChanger.Start();
        }

    /// <summary>
    /// Speed change method. Calls at random Go Faster or Brake
    /// </summary>
        private void ChangeSpeed()
        {
            while (true) //loop to not stop
            {
                if (engine.EngineIsStarted) // while the engine is working
                {
                    //At random pick a value
                    Random r = new Random();
                    int randomNumber = r.Next(1, 4);
                    //2:1 ratio to increase speed
                    if (randomNumber % 3 == 0 || needToStop)
                        Brake(); //breaks
                    else
                    {
                        GoFaster(); // increase speed
                    }
                    if (needToStop) // if the user has stopped the engine or the test drive
                    {
                        StopTestDrive(); //stop the car and end test drive
                    }
                }
                else
                {
                    //finish the thread
                    break;
                }
            }
        }

        /// <summary>
        /// Creates an array of ListViewSubItems to be added for the listview
        /// </summary>
        /// <returns>An array of ListViewSubItems</returns>
        public ListViewItem.ListViewSubItem[] GetListViewItems()
        {
            ListViewItem.ListViewSubItem[] items = new ListViewItem.ListViewSubItem[2 + extras.Count]; //2 car details and extras : engine + color + extras
            ListViewItem.ListViewSubItem value1 = new ListViewItem.ListViewSubItem();
            value1.Text = color.ToString();

            ListViewItem.ListViewSubItem value2 = new ListViewItem.ListViewSubItem();
            value2.Text = engine.ToString();


            items[0] = value1;
            items[1] = value2;
            int index = 2; // put the next values in the array for extras
            foreach (Extra extra in extras)
            {
                ListViewItem.ListViewSubItem value = new ListViewItem.ListViewSubItem();
                value.Text = extra.ToString();
                items[index] = value;
                index++;
            }

            return items;
        }

        /// <summary>
        /// Stops the test drive
        /// </summary>
        internal void StopTestDrive()
        {
            needToStop = true; // flag to stop for Thread
            if (this.speed == 0) // if the car is not moving
            {
                this.IsStarted = engine.StopEngine(); // stop the engine
                
                if (speedChanger != null)//stop the thread if necesery
                    speedChanger.Abort();
            }

        }

        /// <summary>
        /// Changes current image to image with people
        /// </summary>
        public void LoadCarWithPassengers()
        {
            this.image = fullCarImage;
        }

        /// <summary>
        /// Starts the car engine
        /// </summary>
        internal void StartEngine()
        {
            this.IsStarted = engine.StartEngine();
            needToStop = false; //flag to signal no need to stop for test drive
        }

        /// <summary>
        /// Stops the engine and makes sure the speed is not more than 0, the thread does the rest
        /// </summary>
        internal void StopEngine()
        {
            StopTestDrive();
        }

        /// <summary>
        /// Sets the current image to the default 
        /// </summary>
        public void UnloadCarWithPassengers()
        {
            this.image = emptyCarImage;
        }

        /// <summary>
        /// Lowers the current speed by 10, if possible, if not makes it to 0
        /// </summary>
        protected void Brake()
        {
            //not worried for reverse, since it is not implemented
            this.speed -= 10;
            if (this.speed < 0) this.speed = 0;// should not be less than 0 when braking
        }

        /// <summary>
        /// Increases the speed of the car by the speed of the engine.
        /// </summary>
        protected void GoFaster()
        {
            this.speed += this.engine.GiveSpeed();
            if (this.speed > maxSpeed) this.speed = maxSpeed; //limit to the max speed
        }

        /// <summary>
        /// Thread save closing of the thread, must be called if the form is closing
        /// </summary>
        public void AbortThread()
        {
            if (this.speedChanger != null)
            {
                this.speedChanger.Abort();
            }
        }

    }
}
