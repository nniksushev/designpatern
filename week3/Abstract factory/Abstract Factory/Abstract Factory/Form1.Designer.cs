﻿namespace Abstract_Factory
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOrderCar = new System.Windows.Forms.Button();
            this.rbSedan = new System.Windows.Forms.RadioButton();
            this.rbCoupe = new System.Windows.Forms.RadioButton();
            this.rbHatchbag = new System.Windows.Forms.RadioButton();
            this.cbDealership = new System.Windows.Forms.ComboBox();
            this.dealershipType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.butInspect = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOrderCar
            // 
            this.btnOrderCar.Location = new System.Drawing.Point(435, 409);
            this.btnOrderCar.Name = "btnOrderCar";
            this.btnOrderCar.Size = new System.Drawing.Size(97, 39);
            this.btnOrderCar.TabIndex = 0;
            this.btnOrderCar.Text = "Order car";
            this.btnOrderCar.UseVisualStyleBackColor = true;
            this.btnOrderCar.Click += new System.EventHandler(this.btnOrderCar_Click);
            // 
            // rbSedan
            // 
            this.rbSedan.AutoSize = true;
            this.rbSedan.Checked = true;
            this.rbSedan.Location = new System.Drawing.Point(554, 409);
            this.rbSedan.Name = "rbSedan";
            this.rbSedan.Size = new System.Drawing.Size(56, 17);
            this.rbSedan.TabIndex = 1;
            this.rbSedan.TabStop = true;
            this.rbSedan.Text = "Sedan";
            this.rbSedan.UseVisualStyleBackColor = true;
            this.rbSedan.CheckedChanged += new System.EventHandler(this.rbSedan_CheckedChanged);
            // 
            // rbCoupe
            // 
            this.rbCoupe.AutoSize = true;
            this.rbCoupe.Location = new System.Drawing.Point(554, 432);
            this.rbCoupe.Name = "rbCoupe";
            this.rbCoupe.Size = new System.Drawing.Size(56, 17);
            this.rbCoupe.TabIndex = 2;
            this.rbCoupe.TabStop = true;
            this.rbCoupe.Text = "Coupe";
            this.rbCoupe.UseVisualStyleBackColor = true;
            this.rbCoupe.CheckedChanged += new System.EventHandler(this.rbCoupe_CheckedChanged);
            // 
            // rbHatchbag
            // 
            this.rbHatchbag.AutoSize = true;
            this.rbHatchbag.Location = new System.Drawing.Point(554, 455);
            this.rbHatchbag.Name = "rbHatchbag";
            this.rbHatchbag.Size = new System.Drawing.Size(72, 17);
            this.rbHatchbag.TabIndex = 3;
            this.rbHatchbag.TabStop = true;
            this.rbHatchbag.Text = "Hatchbag";
            this.rbHatchbag.UseVisualStyleBackColor = true;
            this.rbHatchbag.CheckedChanged += new System.EventHandler(this.rbHatchbag_CheckedChanged);
            // 
            // cbDealership
            // 
            this.cbDealership.FormattingEnabled = true;
            this.cbDealership.Items.AddRange(new object[] {
            "BMW",
            "Mercedes"});
            this.cbDealership.Location = new System.Drawing.Point(686, 48);
            this.cbDealership.Name = "cbDealership";
            this.cbDealership.Size = new System.Drawing.Size(97, 21);
            this.cbDealership.TabIndex = 6;
            this.cbDealership.SelectedIndexChanged += new System.EventHandler(this.cbDealership_SelectedIndexChanged);
            // 
            // dealershipType
            // 
            this.dealershipType.AutoSize = true;
            this.dealershipType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.dealershipType.Location = new System.Drawing.Point(88, 9);
            this.dealershipType.Name = "dealershipType";
            this.dealershipType.Size = new System.Drawing.Size(457, 65);
            this.dealershipType.TabIndex = 7;
            this.dealershipType.Text = "BMW Dealership";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(690, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 40);
            this.label1.TabIndex = 8;
            this.label1.Text = "select type of dealership";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(2, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(12, 81);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(774, 322);
            this.listView1.TabIndex = 10;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 418);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(337, 54);
            this.label2.TabIndex = 11;
            this.label2.Text = "Select model of a car and click to inspect";
            // 
            // butInspect
            // 
            this.butInspect.Enabled = false;
            this.butInspect.Location = new System.Drawing.Point(15, 444);
            this.butInspect.Name = "butInspect";
            this.butInspect.Size = new System.Drawing.Size(269, 42);
            this.butInspect.TabIndex = 12;
            this.butInspect.Text = "Inspect";
            this.butInspect.UseVisualStyleBackColor = true;
            this.butInspect.Click += new System.EventHandler(this.butInspect_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 494);
            this.Controls.Add(this.butInspect);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dealershipType);
            this.Controls.Add(this.cbDealership);
            this.Controls.Add(this.rbHatchbag);
            this.Controls.Add(this.rbCoupe);
            this.Controls.Add(this.rbSedan);
            this.Controls.Add(this.btnOrderCar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOrderCar;
        private System.Windows.Forms.RadioButton rbSedan;
        private System.Windows.Forms.RadioButton rbCoupe;
        private System.Windows.Forms.RadioButton rbHatchbag;
        private System.Windows.Forms.ComboBox cbDealership;
        private System.Windows.Forms.Label dealershipType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button butInspect;
    }
}

