﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Abstract_Factory;
using Abstract_Factory.Cars;
using System.Windows.Forms;

namespace AbstractFactoryTests
{
    [TestClass]
    public class DealershipTest
    {
        [TestMethod]
        public void CreateDealership()
        {
            CarDealership cd = new CarDealership(TypeOfDealership.BMW);
            Assert.IsNotNull(cd);
        }

        [TestMethod]
        public void OrderCarByCarDealershipBMW()
        {
            CarDealership cd = new CarDealership(TypeOfDealership.BMW);
            Car c = cd.OrderCar(CarTypes.COUPE);
            Assert.AreEqual(typeof(Coupe), c.GetType());
            c.Construct();
            ListViewItem.ListViewSubItem[] var = c.GetListViewItems();
            String engine = "m50b20 - V-12, 6L (540) with Automatic transmition"; // if the car is bmw this should be the engine 
            Assert.AreEqual(engine, var[1].Text.Trim());
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException),
    "An invalid dealership type is given, that is still not implemented.")]
        public void OrderCarByCarDealershipCitroen()
        {
            CarDealership cd = new CarDealership(TypeOfDealership.Citroen);
            Car c = cd.OrderCar(CarTypes.COUPE);
            Assert.AreEqual(typeof(Coupe), c.GetType());

            c.Construct();
            ListViewItem.ListViewSubItem[] var = c.GetListViewItems();

            String engine = "m50b20 - V-12, 6L (540) with Automatic transmition"; // the car is bmw and this should be the engine  since citroen does not exist
            Assert.AreEqual(engine, var[1].Text.Trim());
        }

        [TestMethod]
        public void OrderCarByCarDealershipMercedes()
        {
            CarDealership cd = new CarDealership(TypeOfDealership.Mercedes);
            Car c = cd.OrderCar(CarTypes.COUPE);
            Assert.AreEqual(typeof(Coupe), c.GetType());

            c.Construct();
            ListViewItem.ListViewSubItem[] var = c.GetListViewItems();

            String engine = "merAAA - 2.5L (210) with Automatic transmition"; // if the car is mercedes this should be the engine 
            Assert.AreEqual(engine, var[1].Text.Trim());
        }
    }
}
