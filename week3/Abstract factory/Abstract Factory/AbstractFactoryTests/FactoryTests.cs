﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Abstract_Factory;
using Abstract_Factory.Engines;
using Abstract_Factory.Paints;

namespace AbstractFactoryTests
{
    [TestClass]
    public class FactoryTests
    {
        [TestMethod]
        public void CreateBMWFactory()
        {
            ICarComponentFac fact = new BMWCompFac();

            Assert.AreEqual(typeof(BMWCompFac), fact.GetType());
            Assert.AreEqual("BMW", fact.Type);
        }

        [TestMethod]
        public void CreateMercedesFactory()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Assert.AreEqual("Mercedes", fact.Type);
            Assert.AreEqual(typeof(MercedesCompFac), fact.GetType());
            
        }

        [TestMethod]
        public void CallCreateEngineWithNoTypeOfCar()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Color e = fact.CreateEngine();

            Assert.AreEqual(typeof(BasicMercEng), e.GetType());
           

        }

        [TestMethod]
        public void CallCreateEngineWithSedan()
        {
            ICarComponentFac fact = new MercedesCompFac();
            fact.TypeOfCar = "Sedan";
            Color e = fact.CreateEngine();

            Assert.AreEqual(typeof(B38), e.GetType());


        }

        [TestMethod]
        public void CallCreateEngineWithCoupe()
        {
            ICarComponentFac fact = new MercedesCompFac();
            fact.TypeOfCar = "Coupe";
            Color e = fact.CreateEngine();

            Assert.AreEqual(typeof(MercAAA), e.GetType());

        }

        [TestMethod]
        public void CallCreateEngineWithNoTypeOfCarBMW()
        {
            ICarComponentFac fact = new BMWCompFac();
            Color e = fact.CreateEngine();

            Assert.AreEqual(typeof(BasicBmwEng), e.GetType());


        }

        [TestMethod]
        public void CallCreateEngineWithSedanBMW()
        {
            ICarComponentFac fact = new BMWCompFac();
            fact.TypeOfCar = "Sedan";
            Color e = fact.CreateEngine();

            Assert.AreEqual(typeof(EatHonda), e.GetType());


        }

        [TestMethod]
        public void CallCreateEngineWithCoupeBMW()
        {
            ICarComponentFac fact = new BMWCompFac();
            fact.TypeOfCar = "Coupe";
            Color e = fact.CreateEngine();

            Assert.AreEqual(typeof(EatAMG), e.GetType());


        }


        [TestMethod]
        public void GetPaintMercedes()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Paint e = fact.CreatePaint();

            Assert.AreEqual(typeof(Black), e.GetType());


        }

        [TestMethod]
        public void GetPaintBMW()
        {
            ICarComponentFac fact = new BMWCompFac();
            fact.TypeOfCar = "Sedan";
            Paint e = fact.CreatePaint();

            Assert.AreEqual(typeof(Silver), e.GetType());


        }


    }
}
