﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Abstract_Factory;
using System.Windows.Forms;
using Abstract_Factory.Engines;

namespace AbstractFactoryTests
{
    [TestClass]
    public class CarTests
    {
        [TestMethod]
        public void CreateBMWCoupeCar()
        {
            ICarComponentFac fact = new BMWCompFac();
            Car car = new Abstract_Factory.Cars.Coupe(fact);
            Assert.AreEqual("Coupe", car.Name);
        }
        [TestMethod]
        public void CreateBMWHatchbagCar()
        {
            ICarComponentFac fact = new BMWCompFac();
            Car car = new Abstract_Factory.Cars.Hatchbag(fact);
            Assert.AreEqual("Hatchbag", car.Name);
        }
        [TestMethod]
        public void CreateBMWSedanCar()
        {
            ICarComponentFac fact = new BMWCompFac();
            Car car = new Abstract_Factory.Cars.Sedan(fact);
            Assert.AreEqual("Sedan", car.Name);
        }
        [TestMethod]
        public void CreateMercedesCoupeCar()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Car car = new Abstract_Factory.Cars.Coupe(fact);
            Assert.AreEqual("Coupe", car.Name);
        }
        [TestMethod]
        public void CreateMercedesHatchbagCar()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Car car = new Abstract_Factory.Cars.Hatchbag(fact);
            Assert.AreEqual("Hatchbag", car.Name);
        }
        [TestMethod]
        public void CreateMercedesSedanCar()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Car car = new Abstract_Factory.Cars.Sedan(fact);

            Assert.AreEqual("Sedan", car.Name);
        }

        [TestMethod]
        public void ConstructMercedesSedanCar()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Car car = new Abstract_Factory.Cars.Sedan(fact);
            car.Construct();
            ListViewItem.ListViewSubItem[] var = car.GetListViewItems();
         
          
            String engine = "b38 - V8 4.4L (330) with Automatic transmition";
            Assert.AreEqual(engine, var[1].Text);
            String color = "black";
           Assert.AreEqual(color, var[0].Text);
        }

        [TestMethod]
        public void ConstructMercedesHatchbagCar()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Car car = new Abstract_Factory.Cars.Hatchbag(fact);
            car.Construct();
            ListViewItem.ListViewSubItem[] var = car.GetListViewItems();

            String engine = "MercB - V8 4.4L (330) with Automatic transmition";
            Assert.AreEqual(engine, var[1].Text);
            String color = "black";
            Assert.AreEqual(color, var[0].Text);
            
        }

        [TestMethod]
        public void ConstructMercedesCoupeCar()
        {
            ICarComponentFac fact = new MercedesCompFac();
            Car car = new Abstract_Factory.Cars.Coupe(fact);
            car.Construct();
            ListViewItem.ListViewSubItem[] var = car.GetListViewItems();

            String engine = "merAAA - 2.5L (210) with Automatic transmition";
            Assert.AreEqual(engine, var[1].Text);
            String color = "black";
            Assert.AreEqual(color, var[0].Text);
            

        }

        [TestMethod]
        public void ConstructBMWSedanCar()
        {
            ICarComponentFac fact = new BMWCompFac();
            Car car = new Abstract_Factory.Cars.Sedan(fact);
            car.Construct();
            ListViewItem.ListViewSubItem[] var = car.GetListViewItems();


            String engine = "m44e29 - V8 6.2L (660) with Automatic/Manual transmition";
            Assert.AreEqual(engine, var[1].Text);
            String color = "silver";
            Assert.AreEqual(color, var[0].Text);
        }

        [TestMethod]
        public void ConstructBMWHatchbagCar()
        {
            ICarComponentFac fact = new BMWCompFac();
            Car car = new Abstract_Factory.Cars.Hatchbag(fact);
            car.Construct();
            ListViewItem.ListViewSubItem[] var = car.GetListViewItems();

            String engine = "m33s1 - V6 2.5L (200) with Automatic/Manual transmition";
            Assert.AreEqual(engine, var[1].Text);
            String color = "silver";
            Assert.AreEqual(color, var[0].Text);

        }

        [TestMethod]
        public void ConstructBMWCoupeCar()
        {
            ICarComponentFac fact = new BMWCompFac();
            Car car = new Abstract_Factory.Cars.Coupe(fact);
            car.Construct();
            ListViewItem.ListViewSubItem[] var = car.GetListViewItems();

            String engine = "m50b20 - V-12, 6L (540) with Automatic transmition";
            Assert.AreEqual(engine, var[1].Text.Trim());
            String color = "silver";
            Assert.AreEqual(color, var[0].Text);


        }
    }
}
