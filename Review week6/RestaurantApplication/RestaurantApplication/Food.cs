﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApplication
{
    public class Food
    {
         public string Name;
        public int Quantity;
        public double Price;

        public Food(string name, int quantity, double price)
        {
            this.Name = name;
            this.Price = price;
            this.Quantity = quantity;
        }
        public override string ToString()
        {
            return (Name + " @ " + Price.ToString() + " ," + Quantity.ToString() + " in stock");
        }
    }
}
