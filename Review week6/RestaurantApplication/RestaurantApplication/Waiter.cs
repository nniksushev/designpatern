﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApplication
{
    public class Waiter
    {
        private readonly List<Food> _foodList = new List<Food>(); //available food
        private readonly List<Drink> _drinkList = new List<Drink>(); //available drinks
        private readonly DrinkCommand _drinkCommand;
        private readonly FoodCommand _foodCommand;

        public Waiter(DrinkCommand drinkCommand, FoodCommand foodCommand)
        {
            _drinkCommand = drinkCommand;
            _foodCommand = foodCommand;
        }

        public bool DrinkOrdered(Drink d)
        {
            if (d.Quantity <= 0) // if out of stock can't order
                return false;

            d.Quantity--;
            _drinkCommand.Drink = d; // Make copy of drink
            _drinkCommand.Execute();
            return true;
        }

        public bool FoodOrdered(Food f)
        {
            if (f.Quantity <= 0) // if out of stock can't order
                return false;

            f.Quantity--;
            _foodCommand.Food = f;
            _foodCommand.Execute();
            return true;
        }

        public void AddDrinkToMenu(Drink d)//to create a menu for the restaurant
        {
            _drinkList.Add(d);
        }

        public void AddFoodToMenu(Food f)//to create a menu for the restaurant
        {
            _foodList.Add(f);
        }

        public List<Food> ShowFoodMenu()
        {
            return _foodList;
        }
        public List<Drink> ShowDrinkMenu()
        {
            return _drinkList;
        }
    }
}
