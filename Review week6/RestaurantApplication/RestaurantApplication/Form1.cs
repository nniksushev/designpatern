﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            
            InitializeComponent();
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Create the classes used in the pattern
            Kitchen kitchen = new Kitchen();
            FoodCommand foodCommand = new FoodCommand(kitchen);
            DrinkCommand drinkCommand = new DrinkCommand(kitchen);
            Waiter waiter = new Waiter(drinkCommand, foodCommand);

            Form2 waiterForm = new Form2(waiter);
            waiterForm.Show();

            Form3 kitchenFrom = new Form3(kitchen);
            kitchenFrom.Show();
        }
    }
}
