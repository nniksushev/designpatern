﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApplication
{
    public interface IReceiver
    {
        void OnDrinkReceived(Drink dc);
        void OnFoodReceived(Food fc);
    }
}
