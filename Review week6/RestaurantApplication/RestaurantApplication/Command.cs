﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApplication
{
    public abstract class Command
    {
        protected IReceiver Receiver; // All the commands notify the IReceiver on Execute()

        protected Command(IReceiver receiver)
        {
            Receiver = receiver;
        }

        public virtual void Execute() { }

    }
}
