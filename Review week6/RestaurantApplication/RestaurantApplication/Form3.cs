﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class Form3 : Form
    {
        private readonly Kitchen _kitchen;
        
        public Form3(Kitchen k)
        {
            InitializeComponent();
            _kitchen = k;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox3.Items.Clear();
            foreach (var item in _kitchen.ListOfFoodOrders)
            {
                listBox1.Items.Add(item);
                
            }
            foreach (var item in _kitchen.ListOfDrinkOrders)
            {
                listBox3.Items.Add(item);

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //kictchen should execute the order
            //listBox2.Items.Clear();
            //foreach (var item in kitchen.ListOfFoodOrders)
            //{
            //    listBox2.Items.Add(item.ToString() + " is ready");
            //    //kitchen.ListOfFoodOrders.Remove(item);
            //}
            //foreach (var item in kitchen.ListOfDrinkOrders)
            //{
            //    listBox2.Items.Add(item.ToString() + " is ready");
            //    //kitchen.ListOfDrinkOrders.Remove(item);
            //}
            //kitchen.ListOfDrinkOrders.Clear();
            //kitchen.ListOfFoodOrders.Clear();
            //listBox1.Items.Clear();
            listBox2.Items.Clear();

            Food f = listBox1.SelectedItem as Food;
            Drink d = listBox3.SelectedItem as Drink;

            _kitchen.ProcessOrder(f, d);

            if (f != null) // If food is selected
            {
                listBox2.Items.Add(listBox1.SelectedItem + " is ready");
                listBox1.Items.Remove(listBox1.SelectedItem);
            }

            if (d != null)
            {
                listBox2.Items.Add(listBox3.SelectedItem + " is ready");
                listBox3.Items.Remove(listBox3.SelectedItem);
            }
        }
    }
}
