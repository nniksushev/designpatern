﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApplication
{
    public class Kitchen : IReceiver
    {
        public List<Food> ListOfFoodOrders = new List<Food>();
        public List<Drink> ListOfDrinkOrders = new List<Drink>();


        public void OnDrinkReceived(Drink dc)
        {
            ListOfDrinkOrders.Add(dc);
        }

        public void OnFoodReceived(Food fc)
        {
            ListOfFoodOrders.Add(fc);
        }

        public void ProcessOrder(Food f, Drink d)
        {
            ListOfDrinkOrders.Remove(d);
            ListOfFoodOrders.Remove(f);
        }
    }
}
