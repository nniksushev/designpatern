﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestaurantApplication
{
    public partial class Form2 : Form
    {
        private Waiter _waiter;

        public Form2(Waiter waiter)
        {
            InitializeComponent();
            _waiter = waiter;

            Drink dr1 = new Drink("cola", 10, 1.50);
            _waiter.AddDrinkToMenu(dr1);
            Drink dr2 = new Drink("fanta", 20, 1.50);
            _waiter.AddDrinkToMenu(dr2);
            Drink dr3 = new Drink("beer", 25, 3.0);
            _waiter.AddDrinkToMenu(dr3);
            Drink dr4 = new Drink("orange juice", 10, 2.0);
            _waiter.AddDrinkToMenu(dr4);
           

            Food food1 = new Food("grilled chicken with fries" , 20, 10.80);
            _waiter.AddFoodToMenu(food1);
            Food food2 = new Food("chicken salad", 15, 5.20);
            _waiter.AddFoodToMenu(food2);
            Food food3 = new Food("pasta with minced meat", 10, 9.20);
            _waiter.AddFoodToMenu(food3);
            Food food4= new Food("stir fried rice and beef", 20, 8.0);
            _waiter.AddFoodToMenu(food4);
            Food food5 = new Food("ribs with fries", 10, 10.80);
            _waiter.AddFoodToMenu(food5);

            UpdateLists();
        }

        public void UpdateLists()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            foreach (Food item in _waiter.ShowFoodMenu())
            {
                listBox1.Items.Add(item.ToString());
            }
            foreach (Drink item in _waiter.ShowDrinkMenu())
            {
                listBox2.Items.Add(item.ToString());
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int indexF = listBox1.SelectedIndex;
            if (!_waiter.FoodOrdered(_waiter.ShowFoodMenu()[indexF]))
                MessageBox.Show("Sorry product out of stock");

            UpdateLists();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            int indexD = listBox2.SelectedIndex;

            if (!_waiter.DrinkOrdered(_waiter.ShowDrinkMenu()[indexD]))
                MessageBox.Show("Sorry product out of stock");

            UpdateLists();
        }
    }
}
