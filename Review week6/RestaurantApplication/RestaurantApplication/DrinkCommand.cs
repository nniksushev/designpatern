﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApplication
{
    public class DrinkCommand: Command
    {
        public Drink Drink { get; set; }
        
        
        public override void Execute()
        {
            Receiver.OnDrinkReceived(Drink);//call the kitchen to execute the command
        }
        public  override string ToString()
        {
            return (Drink.Name + " , @" + Drink.Price);
        }

        public DrinkCommand(IReceiver receiver) : base(receiver)
        {
        }
    }
}
