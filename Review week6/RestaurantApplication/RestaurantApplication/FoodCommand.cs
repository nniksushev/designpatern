﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApplication
{
    public class FoodCommand: Command
    {

        public Food Food { get; set; }
       

        public override void Execute()
        {
            Receiver.OnFoodReceived(Food);//call the kitchen to exexute the command
        }
        public override string ToString()
        {
            return( Food.Name+" , @"+Food.Price);
        }

        public FoodCommand(IReceiver receiver) : base(receiver)
        {
        }
    }
}
