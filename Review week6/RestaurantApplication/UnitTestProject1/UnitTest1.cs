﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantApplication;

namespace UnitTestProject1
{
    
    [TestClass]
    public class UnitTest1
    {
        private Kitchen kitchen = new Kitchen();
        private FoodCommand foodCommand;
        private DrinkCommand drinkCommand;
        private Waiter waiter;
        private Drink testdrink = new Drink("Fanta", 1, 2);
        private Food testfood = new Food("Bread", 1, 3);
        [TestMethod]
        public void PlaceOrder()
        {
            foodCommand = new FoodCommand(kitchen);
            drinkCommand = new DrinkCommand(kitchen);
            waiter = new Waiter(drinkCommand, foodCommand);

            Assert.AreEqual(kitchen.ListOfDrinkOrders.Count, 0);
            Assert.AreEqual(kitchen.ListOfFoodOrders.Count, 0);
            
            foodCommand.Food = testfood;
            drinkCommand.Drink = testdrink;

            foodCommand.Execute();
            drinkCommand.Execute();

            Assert.AreEqual(kitchen.ListOfDrinkOrders.Count, 1);
            Assert.AreEqual(kitchen.ListOfFoodOrders.Count, 1);

            //test ProcessOrder
            kitchen.ProcessOrder(testfood, testdrink);

            Assert.AreEqual(kitchen.ListOfDrinkOrders.Count, 0);
            Assert.AreEqual(kitchen.ListOfFoodOrders.Count, 0);
        }

       
    }
}
