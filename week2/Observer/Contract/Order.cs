﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ExchangeLibrary
{

    [DataContract, Serializable]
    [KnownType(typeof(Order))]
    [KnownType(typeof(StockObserver))]// for serialization
 
    public class Order
    {
       
        public Order(String name)
        {
            this.name = name;
        }
        [NonSerialized]
        private string name;

        [DataMember]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        
        public override string ToString()
        {
            return name;
        }


    }
}
