﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
namespace ExchangeLibrary
{

    [ServiceContract(Namespace = "ExchangeLibrary")]
    
       
    public interface IStockObserver : ISerializable
    {
        [OperationContract]            
        bool Publish(Order order, IStockObserver byWho);
        [OperationContract]
        bool Buy(Order order, IStockObserver fromWho, IStockObserver byWho);
        [OperationContract]
        void Update();
        [OperationContract]
        void ConnectOrDisconnect(IStockObserver client);
    }
}
