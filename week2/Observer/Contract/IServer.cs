﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExchangeLibrary;
using System.ServiceModel;

namespace ExchangeLibrary
{
  
    [ServiceContract]
    public interface IServer : IStockObserver
    {
        [OperationContract]
        void Attach(IStockObserver client);
        [OperationContract]
        void Detach(IStockObserver client);
        [OperationContract]
        void Notify();
    }
}
