﻿using ExchangeLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace ExchangeLibrary
{
    [ Serializable]
    [KnownType(typeof(Order))]
    [KnownType(typeof(StockObserver))]// for serialization
    [KnownType(typeof(IStockObserver))]// for serialization
    public class StockObserver : ExchangeLibrary.IStockObserver
    {
        private String name;
        private List<Order> orders;
        
        
      
        public List<Order> Orders
        {
            get { return orders; }
            
        }
      
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Used when sending the by who
        /// </summary>
        /// <param name="name"></param>
        public StockObserver(String name)
        {
            this.name = name;
        }

        public StockObserver(SerializationInfo info,
        StreamingContext context)
            
     { }


        public bool Publish(Order order, IStockObserver byWho)
        {
            throw new NotImplementedException();
        }

        public bool Buy(Order order, IStockObserver fromWho, IStockObserver byWho)
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void ConnectOrDisconnect(IStockObserver client)
        {
            throw new NotImplementedException();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("name", name);
            info.AddValue("orders", orders);
        }
    }
}
