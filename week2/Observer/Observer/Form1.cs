﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExchangeLibrary;

namespace Observer
{
    public partial class Form1 : Form
    {
        ExchangeService.StockObserverClient server;
        IStockObserver ob;
        
        public Form1()
        {
            InitializeComponent();

            /*( WSHttpBinding binding = new WSHttpBinding();
             Uri address = new Uri("http://localhost:8000/exchange");
             EndpointAddress endpointAddress = new EndpointAddress(address);*/
            server = new ExchangeService.StockObserverClient();
            ob = new StockObserver("Nikola");
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            IStockObserver ob2 = new StockObserver("lupendi");//will be retieved  from  server. might be changed
            Order order = new Order("Pickles");
            server.Buy(order, ob, null);
        
        }
    }
}
