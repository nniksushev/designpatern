﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Observer.ExchangeService;
using ExchangeLibrary;

namespace Observer
{
    
    public class ServerConnection : ClientBase<ExchangeService.IStockObserver>
    {
        public ServerConnection()
        {
            
        }

        public ServerConnection(string endpointConficugrationName) : base(endpointConficugrationName)
        { }

        public ServerConnection(string endpointConficugrationName, string remoteAddress)
                : base(endpointConficugrationName, remoteAddress)
        { }

        public ServerConnection(string endpointConficugrationName, EndpointAddress remoteAddress) :
                base(endpointConficugrationName, remoteAddress)
        { }

        public ServerConnection(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) :
               base(binding, remoteAddress)
        { }


        public bool Buy(Order order, ExchangeLibrary.IStockObserver fromWho, ExchangeLibrary.IStockObserver byWho)
        {
            return base.Channel.Buy(order, fromWho, byWho);
        }

        public bool Sell(Order order, ExchangeLibrary.IStockObserver byWho)
        {
            return base.Channel.Publish(order, byWho);
        }

        public void Update()
        {
            base.Channel.Update();
        }

        public void ConnectOrDisconnect()
        {
            base.Channel.ConnectOrDisconnect(this);
        }

        public override string ToString()
        {
            return "Steven";
        }

    }
}
