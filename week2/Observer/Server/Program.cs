﻿using System;
using System.ServiceModel;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {

            ServiceHost host = new ServiceHost(typeof(StockServer));

            //Type contract = typeof(ICalculator);
            //WSHttpBinding binding = new WSHttpBinding();
            //Uri address = new Uri("http://localhost:8000/calculatorservice");
            //host.AddServiceEndpoint(contract, binding, address);

            host.Open();

            Console.WriteLine("The trade center is operational!");
            Console.WriteLine("Press <ENTER> to stop the server.\n");
            Console.ReadLine();

            host.Close();
        }
    }
}
