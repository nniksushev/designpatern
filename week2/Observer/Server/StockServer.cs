﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;


using ExchangeLibrary;
using System.Runtime.Serialization;

namespace Server
{
    /// <summary>
    /// Class for the server. It contains the implementation of the methods to communicate between the client and the server.
    /// </summary>

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]

    public class StockServer : IServer
    {
        List<IStockObserver> clients;
        List<Order> orders;

        public StockServer()
        {
            this.clients = new List<IStockObserver>();
            this.orders = new List<Order>();
        }
        /// <summary>
        /// Adds a new client to be tracked
        /// </summary>
        /// <param name="client">The client to be added</param>
        public void Attach(IStockObserver client)
        {
            //Todo Need to make sure the client does not exist.
            this.clients.Add(client);
            Console.WriteLine("Client added");
        }


        /// <summary>
        ///   /// Sends a buy request from a client
        /// </summary>
        /// <param name="order">The order that is getting bought</param>
        /// <param name="fromWho">The client sending the buy request</param>
        /// <returns></returns>
        public bool Buy(Order order, IStockObserver fromWho, IStockObserver byWho)
        {
            
            Console.WriteLine("i bought this");
            Console.WriteLine(order.ToString());
            Console.WriteLine(fromWho.ToString());
            return true;
            
        }

        /// <summary>
        /// Removes a client from the list of clients to be tracked.
        /// </summary>
        /// <param name="client">The client that will be removed</param>
        public void Detach(IStockObserver client)
        {
            //Todo Need to make sure the client exists.
            this.clients.Remove(client);
        }

        /// <summary>
        /// Sends a notification to all the clients that wish to be notified about the client change.
        /// </summary>
        public void Notify()
        {
            foreach (IStockObserver client in this.clients)
            {
                client.Update();
            }
            
        }

        /// <summary>
        /// Releases a new order that the client is selling.
        /// </summary>
        /// <param name="order">The order that the client is selling</param>
        /// <returns></returns>
        public bool Publish(Order order, IStockObserver byWho)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates the server from the change.
        /// ???MAYBE NOT NECESERY
        /// </summary>
        public void Update()
        {
            throw new NotImplementedException();
        }

        public String PrintAllClients()
        {
            return "";
        }

        public void ConnectOrDisconnect(IStockObserver client)
        {
            if (!clients.Contains(client))
            {
                Attach(client);
                Console.WriteLine("Client added");
                Console.WriteLine(client.ToString());
            }
            else
            {
                Detach(client);
                Console.WriteLine("Client removed");
                Console.WriteLine(client.ToString());
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}
