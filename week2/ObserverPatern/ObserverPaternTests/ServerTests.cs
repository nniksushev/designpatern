﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObserverPatern;
namespace ObserverPaternTests
{
    [TestClass]
    public class ServerTests
    {
        [TestMethod]
        public void CreateServer()
        {
            IServer server = new StockServer(true);

            Assert.IsNotNull(server);
        }

        [TestMethod]
        public void ServerAttachObserver()
        {
            IServer server = new StockServer(true);
            server.Attach(new StockObserverPush(server, "bob", null));

            int count = (server as StockServer).GetObservers().Count;
            
            Assert.AreEqual(4, count);
            IStockObserverPush obs = (server as StockServer).GetObservers()[0];
            String expectedName = "Josephi";
            Assert.AreEqual(expectedName, obs.Name);
        }

        [TestMethod]
        public void ServerAttachDetachObserver()
        {
            IServer server = new StockServer(true);
            IStockObserverPush obs = new StockObserverPush(server, "bob", null);
            server.Attach(obs);

            int count = (server as StockServer).GetObservers().Count;

            Assert.AreEqual(4, count);

            server.Detach(obs);
            count = (server as StockServer).GetObservers().Count;

            Assert.AreEqual(3, count);
            
        }

        [TestMethod]
        public void AddStockToServer()
        {
            StockServer server = new StockServer(true);
            Stock stock = new Stock("pickles", 1.5);
            server.Sell(stock, null);


            Assert.AreEqual(1, server.GetStock().Count);

            Assert.AreEqual("pickles", server.GetStock()[0].Name);

             stock = new Stock("pickles2", 1.5);
            server.Sell(stock, null);


            Assert.AreEqual(2, server.GetStock().Count);
        }

        [TestMethod]
        public void SellFromStockStockToServer()
        {
            StockServer server = new StockServer(true);
            Stock stock = new Stock("pickles", 1.5);
            stock.Quantity = 3;
            server.Sell(stock, null);


            Assert.AreEqual(1, server.GetStock().Count);

            Assert.AreEqual(3, server.GetStock()[0].Quantity);
            stock.Quantity = 6;
            server.Sell(stock, null);
            Assert.AreEqual(12, server.GetStock()[0].Quantity);
        }


        [TestMethod]
        public void BuyFromStockStockToServer()
        {
            StockServer server = new StockServer(true);
            Stock stock = new Stock("pickles", 1.5);
            stock.Quantity = 3;
            server.Sell(stock, null);


            Assert.AreEqual(1, server.GetStock().Count);

            Assert.AreEqual(3, server.GetStock()[0].Quantity);
            stock.Quantity = 2;
            server.Buy(stock, null);
            Assert.AreEqual(0, server.GetStock()[0].Quantity);
        }

        [TestMethod]
        public void BuyFromStockAll()
        {
            StockServer server = new StockServer(true);
            Stock stock = new Stock("pickles", 1.5);
            stock.Quantity = 3;
            server.Sell(stock, null);


            Assert.AreEqual(1, server.GetStock().Count);

            Assert.AreEqual(3, server.GetStock()[0].Quantity);
            stock.Quantity = 3;
            server.Buy(stock, null);
            Assert.AreEqual(0, server.GetStock()[0].Quantity);
        }

        [TestMethod]
        public void BuyFromStockTooMuch()
        {
            StockServer server = new StockServer(true);
            Stock stock = new Stock("pickles", 1.5);
            stock.Quantity = 3;
            bool result = server.Sell(stock, null);

            Assert.IsTrue(result);
            Assert.AreEqual(1, server.GetStock().Count);

            Assert.AreEqual(3, server.GetStock()[0].Quantity);
            stock.Quantity = 6;
            server.Buy(stock, null);
            Assert.AreEqual(0, server.GetStock()[0].Quantity);
        }

        [TestMethod]
        public void BuyFromStockNotExisting()
        {
            StockServer server = new StockServer(true);
            Stock stock = new Stock("pickles", 1.5);
            stock.Quantity = 3;
            bool result = server.Buy(stock, null);


            Assert.AreEqual(0, server.GetStock().Count);
            Assert.IsFalse(result);
          
        }

        [TestMethod]
        public void SellChangePrice()
        {
            StockServer server = new StockServer(true);
            Stock stock = new Stock("pickles", 1.5);
            stock.Quantity = 3;
            server.Sell(stock, null);
            

            Assert.AreEqual(1, server.GetStock().Count);

            Assert.AreEqual(1.5, server.GetStock()[0].Price);
            stock = new Stock("pickles", 5);
            server.Sell(stock, null);
            Assert.AreEqual(5, server.GetStock()[0].Price);
            Assert.AreEqual(-3.5, server.GetStock()[0].Change);
        }

        [TestMethod]
        public void UpdateStockPrice()
        {
            StockServer server = new StockServer(true);
            Stock stock = new Stock("pickles", 1.5);
            stock.Quantity = 3;
            server.Sell(stock, null);


            Assert.AreEqual(1, server.GetStock().Count);

            Assert.AreEqual(1.5, server.GetStock()[0].Price);
            stock = new Stock("pickles", 5);
            server.Sell(stock, null);
            Assert.AreEqual(5, server.GetStock()[0].Price);
            Assert.AreEqual(-3.5, server.GetStock()[0].Change);
        }
    }
}
