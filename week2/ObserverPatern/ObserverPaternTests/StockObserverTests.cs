﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObserverPatern;
namespace ObserverPaternTests
{
    [TestClass]
    public class StockObserverTests
    {
        [TestMethod]
        public void CreateAnObserver()
        {
            IStockObserverPush obs = new StockObserverPush(null, "Nikola", null);
            Assert.AreEqual("Nikola", obs.Name);
            
        }
    }
}
