﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObserverPatern;

namespace ObserverPaternTests
{
    [TestClass]
    public class StockTests
    {
        [TestMethod]
        public void CreateAStockWithName()
        {
            Stock s = new Stock("Pickles");

            String expectedName = "Pickles";
            Assert.IsNotNull(s);
            Assert.AreEqual(expectedName, s.Name);
        }

        [TestMethod]
        public void CreateAStockWithNameAndPrice()
        {
            Stock s = new Stock("Pickles", 3.5);
            Assert.IsNotNull(s);

            String expectedName = "Pickles";
            Assert.AreEqual(expectedName, s.Name);

            double expectedPrice = 3.5;
            Assert.AreEqual(expectedPrice, s.Price);
        }


        [TestMethod]
        public void AlterStockQuantity()
        {
            Stock s = new Stock("Pickles", 3.5);

            s.Quantity = 3;

            int expectedQuantity = 3;
            Assert.AreEqual(expectedQuantity, s.Quantity);

            s.IncreaseQuantityBy(3);
            expectedQuantity = 6;
            Assert.AreEqual(expectedQuantity, s.Quantity);

            s.ReduceQuantityBy(2);
            expectedQuantity = 4;
            Assert.AreEqual(expectedQuantity, s.Quantity);
        }

        [TestMethod]
        public void AlterStockPrice()
        {
            Stock s = new Stock("Pickles", 3.5);
            
            s.ChangePrice(5);
            double expectedPrice = 5;
            Assert.AreEqual(expectedPrice, s.Price);

            double expectedChange = -1.5;
            Assert.AreEqual(expectedChange, s.Change);
        }
    }
}
