﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatern
{
    //Since we are using Windows forms and a fake server we must implement the class with a custom
    //server and a custom form. The server is to know where to send the connection requests and 
    //other requests. The form is to indicate where the graphical interface will be.


    /// <summary>
    /// Observer that will be updated when there is a change. Implements IStockObserver
    /// </summary>
   public class StockObserverPull : IStockObserverPull
    {
      
        IServer server; //Server is used to keep location of the buy and sell/ as a reference to the address
        private List<Stock> stocks;
        private String name;
        private Form1 form; //Used to indicate to the form to update the content
        public String Name
        {
            get { return name; }
           
        }

        /// <summary>
        /// Constructor for an observer
        /// </summary>
        /// <param name="server">The server that will be connected to the user</param>
        /// <param name="name">Name of the observer</param>
        /// <param name="form">The form that will be getting user interface updates</param>
        public StockObserverPull(IServer server, String name, Form1 form)
        {
            this.form = form;
            this.server = server;
            this.name = name;
            this.stocks = new List<Stock>();
        }

        /// <summary>
        /// Sends a buy request to the server
        /// </summary>
        /// <param name="stock">The stock to be bought</param>
        /// <param name="byWho">By who, ( this )</param>
        /// <returns>Boolean if the transaction was succesful</returns>
        public bool Buy(Stock stock, ITransaction byWho)
        {
            //Validates the stock is present in the current user list(it exists).
            if (Utilities.StockIsPresent(stock, stocks))
            {
                (server as StockServer).Buy(stock, byWho);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sells a stock to the server
        /// </summary>
        /// <param name="stock">The stock to be sold</param>
        /// <param name="byWho">By who, (this)</param>
        /// <returns>boolean if the transaction was succesful</returns>
        public bool Sell(Stock stock, ITransaction byWho)
        {
            //no need to check if it exists, server makes the check and adds it if it not existing.
            return (server as StockServer).Sell(stock, byWho);
        }

        /// <summary>
        /// Updates the current observer stock list and sends a form request to update the interface
        /// </summary>
        /// <param name="stocks">The list of new stocks</param>
        public void Update(IServer server)
        {
            this.stocks = server.GetStock();
            if (form != null)//if the form is not initalized, no need to update
                form.UpdateInterface(stocks);
        }

        /// <summary>
        /// Closing the form makes the user form to null
        /// </summary>
        public void ClosedForm()
        {
            this.form = null;
        }
    }
}
