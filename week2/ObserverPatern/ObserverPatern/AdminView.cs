﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ObserverPatern
{
    public partial class AdminView : Form
    {
        private static IServer server;
        System.Threading.Thread updateThread;
        public AdminView()
        {
            InitializeComponent();
            server = new StockServer(false);
            updateThread = new System.Threading.Thread(new System.Threading.ThreadStart(UpdateList));
        }

        private void UpdateList()
        {
            while (true)
            {
                
                Thread.Sleep(2000);
                this.Invoke(new Action(() =>listBox1.Items.Clear()));
                foreach (IStockObserverPush obs in (server as StockServer).GetObservers())
                {
                    this.Invoke(new Action(() =>listBox1.Items.Add(obs.Name)));
                }
            }
        }

        public AdminView(IServer serverNew)
        {
            InitializeComponent();
            server = serverNew;
        }



        private void AdminView_Load(object sender, EventArgs e)
        {
            updateThread.Start();
        }

        public static void UpdateServer(IServer serverNew)
        {
            server = serverNew;
            
        }

        private void AdminView_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1.AdminViewClosed();
            ((server as StockServer)).StopServer();
            updateThread.Abort();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Form1 form1 = new Form1(textBox1.Text, server, false);
            
            form1.Show();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                String value = listBox1.SelectedItem.ToString();
                Form1 form1 = new Form1(value, server, true);
                form1.Show();
            }
        }
    }
}
