﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatern
{
    /// <summary>
    /// Stock server that implements IServer. Contains the logic for the server
    /// </summary>
    public class StockServer : IServer
    {

        private static List<IStockObserverPush> observers; //We are only interested in the Push observers so we can  notify them when there is a change.
        private static List<Stock> stocks;
        private RandomEventGenerator rEG; //Used to attach a random event generator
        private bool changed; //Updated when there is a change.

        /// <summary>
        /// Inializes the start data and the server
        /// </summary>
        public StockServer(bool forTesting)
        {

            stocks = new List<Stock>();
            if (!forTesting) //only for testing perposes, in the real product the Testing parameter is not included
            {
                //Init starting data of stocks
                Stock stock1 = new Stock("SunEdison Inc", 2.26);
                stock1.Quantity = 5;
                stock1.ChangePrice(3.0);
                stock1.ChangePrice(3.1);
                Stock stock2 = new Stock("Oil", 32.41);
                stock2.Quantity = 12;
                Stock stock3 = new Stock("Dow", 1667.97);
                stock3.ChangePrice(179.99);
                stock3.Quantity = 17;
                //end init data for stock
                stocks = new List<Stock>() { stock1, stock2, stock3 };// puts the data in the list
            }
            observers = new List<IStockObserverPush>();
            rEG = new RandomEventGenerator(this);//creates a new random event generator with the server passes as a paramater.

        }

        

        /// <summary>
        /// Attaches a listener to the server to notify the push user.
        /// </summary>
        /// <param name="observer">Type of IStockObserver that will be registered to the server for updates.</param>
        public void Attach(IStockObserverPush observer)
        {
            //checks if the user is already connected
            if (!Utilities.ObserverIsPresent(observer, observers))
                observers.Add(observer); //user is still not connected, adds a new
            else
            {
                //user is connected, so the observer to which the data must be sent is updated.
                observers[observers.IndexOf(observers.Find(x => x.Name == observer.Name))] = observer;
            }
            //sends an initial update to the observer stock
            observer.Update(stocks);
        }

        /// <summary>
        /// Buys a stock from the server.
        /// </summary>
        /// <param name="stock">The stock that will be bought.</param>
        /// <param name="byWho">Who bought the stock.</param>
        /// <returns>Boolean indicating if the purchase was succesful.</returns>
        public bool Buy(Stock stock, ITransaction byWho)
        {
            if (Utilities.StockIsPresent(stock, stocks))
            {
                int index = stocks.IndexOf(stocks.Find(x=> x.Name == stock.Name));//gets the index of the stock
                Stock currentStock = stocks[index];

                //produces a message if the purchase was succesful or not
                String message = currentStock.ReduceQuantityBy(stock.Quantity);
                SetChanged(message); //enables the notification
                Notify();//sends a notification
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes an observer from the list of observers
        /// </summary>
        /// <param name="observer">Type of IStockObserver that will be removed from the list</param>
        public void Detach(IStockObserverPush observer)
        {

            if (Utilities.ObserverIsPresent(observer, observers))
                observers.Remove(observer);
        }
        /// <summary>
        /// Sends a notification to all the registered Push observers
        /// </summary>
        public void Notify()
        {
            try
            { //to make sure the user is not playing too much with the change of clients
                if (changed) //if there was a change made
                {
                    foreach (IStockObserverPush obs in observers)
                    {
                        //update all observers stock
                        obs.Update(stocks);
                    }
                }
                changed = false;
            }
            catch (InvalidOperationException)
            {
                //The user switched his users too much...
                //we wish to continue the normal flow, so we just record the error(no database to record)
            }
        }


        /// <summary>
        /// Sells a specific stock to the market, increasing the already exsisting or adding a new to the market.
        /// </summary>
        /// <param name="stock">The stock that is sold</param>
        /// <param name="byWho">Who sold the stock</param>
        /// <returns></returns>
        public bool Sell(Stock stock, ITransaction byWho)
        {
            String message = String.Empty;
            if (Utilities.StockIsPresent(stock, stocks))
            {
                //the stock is sold, and increase the current stock quantity
                int index = stocks.IndexOf(stocks.Find(x => x.Name == stock.Name));
                
                stocks[index].Quantity += stock.Quantity;
                stocks[index].ChangePrice(stock.Price);
                message = ("Stock sold. (" + stock.Name + ")");
            }
            else
            {
                //the stock is not present, so it is added to the stock list
                stocks.Add(stock);
                message = ("Stock added. (" + stock.Name + ")");


            }
            SetChanged(message);//enables notify
            Notify();//notify push observers

            return true;
        }

        /// <summary>
        /// Changes the current change flag to true
        /// </summary>
        /// <param name="message">A message to be printed to the user, currently not used</param>
        void SetChanged(String message)
        {
            //System.Windows.Forms.MessageBox.Show(message, "New Update");
            changed = true;
        }

        /// <summary>
        /// Retrieves the list of current stocks from the server
        /// </summary>
        /// <returns>List of Stocks from the server</returns>
        public List<Stock> GetStock()
        {
            return stocks;
        }

        /// <summary>
        /// Updates a stock's price
        /// </summary>
        /// <param name="stock">The stock to be updated</param>
        /// <param name="price">The price to be changed to</param>
        public void SetStock(Stock stock, double price)
        {
            foreach (Stock st in stocks)
            {
                //validates the stock is present in the list
                if (st == stock)
                {

                    st.ChangePrice(price);
                    SetChanged("Price changed. (" + stock.Name + ")"); //set change to true
                    Notify();//notify observers
                    break;
                }
            }
        }


        /// <summary>
        /// Stops the second thread for the random generator
        /// </summary>
        public void StopServer()
        {
            rEG.StopThread();
        }

        /// <summary>
        /// Retunrs the servers observers, used for the admin View
        /// </summary>
        /// <returns>List of IStockObserver</returns>
        public List<IStockObserverPush> GetObservers()
        {
            return observers;
        }

    }


}
