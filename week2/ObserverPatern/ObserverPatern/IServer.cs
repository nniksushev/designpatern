﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatern
{
    public interface IServer 
    {
      
        void Attach(IStockObserverPush client);

        void Detach(IStockObserverPush client);

        void Notify();
        List<Stock> GetStock();
    }
}
