﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatern
{
    public interface IStockObserverPush : ITransaction
    {
    
        void Update(List<Stock> stocks);

        

    }
}
