﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObserverPatern
{
    /// <summary>
    /// Class that holds the information about a stock
    /// </summary>
    public class Stock
    {


        private string name;
        private double price;//current price
        private double startPrice; //starting price
       
        private int quantity;

        /// <summary>
        /// Creates a stock with a name, must be initialized later on in the program.
        /// </summary>
        /// <param name="name">Name of the stock</param>
        public Stock(String name)
        {
            this.name = name;
        }

        /// <summary>
        /// Creates a stock with a price and a name
        /// </summary>
        /// <param name="name">Name of the stock</param>
        /// <param name="price">Starting price of the stock</param>
        public Stock(string name, double price) : this(name)
        {
            this.price = price;
            this.startPrice = price;
        }



        public int Quantity
        {
            get { return quantity; }
            set { this.quantity = value; }
        }


        public double Price
        {
            get { return price; }

        }

        public string Name
        {
            get { return name; }

        }
        public double Change
        {
            get
            {
                return startPrice - price;//difference between start and current price
            }
        }

        /// <summary>
        /// Displays the stock information in format Stock : name    |    Price: 0.00     |   Change: 0.00{2} 
        /// {2} is an arrow up or down  
        /// </summary>
        /// <returns>Formatted stock</returns>
        public override string ToString()
        {
            string arrow = "";
            if (Change < 0)
                arrow = "\u2193";
            else if (Change > 0)
                arrow = "\u2191";
            return String.Format("Stock : {0}     |     Price: {1:0.00}     |     Change: {2:0.00}{3}", name, price, Change, arrow);
        }

        /// <summary>
        /// Changes the price to a new value
        /// </summary>
        /// <param name="pricev">New price</param>
        public void ChangePrice(double pricev)
        {

            this.price = pricev;

        }

        /// <summary>
        /// Reduces the quantity of a stock and returns a message if possible or not.
        /// </summary>
        /// <param name="quantity">The quantity to remove</param>
        /// <returns>String with the success of the operation</returns>
        public String ReduceQuantityBy(int quantity)
        {
            if (this.quantity >= quantity)
            {
                //Possible to remove from the stock
                this.quantity -= quantity;
                return "Stock bought!(" + this.name + ")";
            }
            else
            {
                //The number given was too big
                return "Not enough quantity! We currently have " + quantity + " for " + name;
            }

        }

        /// <summary>
        /// Increase the quantity of the stock by a specific number.
        /// </summary>
        /// <param name="quantity">The quantity to increase by</param>
        /// <returns>String that says if the operation was succesful.</returns>
        public String IncreaseQuantityBy(int quantity)
        {
            this.quantity += quantity;
            return "Increased quantity";
        }

        /// <summary>
        /// Gets the current difference as a string and an arrow
        /// </summary>
        /// <returns>Change in formaat 0.00{2}
        /// {2} being an arrow up or down</returns>
        internal String GetChange()
        {
            string arrow = "";
            if (Change < 0)
                arrow = "\u2193";
            else if (Change > 0)
                arrow = "\u2191";
            return String.Format("{0:0.00}", this.Change) + arrow;
        }
    }

}
