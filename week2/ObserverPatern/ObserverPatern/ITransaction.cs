﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatern
{
   public interface ITransaction
    {
        bool Sell(Stock stock, ITransaction byWho);

        bool Buy(Stock stock, ITransaction byWho);

        String Name { get; }
    }
}
