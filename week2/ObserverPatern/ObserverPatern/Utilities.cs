﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatern
{
    /// <summary>
    /// Class which holds functions to see if an observer is present in a list or if a stock is present in a list,
    /// used by the Stock Server.
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// Checks if a observer is present in a list of  IStockObserver observers
        /// </summary>
        /// <param name="observer">Type IStockObserver that will be checked if he is present in the list</param>
        /// <param name="observers">List Type IStockObserver which has all the observers inside</param>
        /// <returns></returns>
        public static bool ObserverIsPresent(IStockObserverPush observer, List<IStockObserverPush> observers)
        {
            if (observers == null) return false; //if the list is empty no need to continue
            foreach (IStockObserverPush obs in observers)
            {
                //Checks by name of observer
                if (obs.Name.Equals(observer.Name)) {
                    return true;
                }
            }
           
            return false;
                
        }

        /// <summary>
        /// Checks if a stock is present in a list of Stocks.
        /// </summary>
        /// <param name="stock">The stock that is being searched for.r</param>
        /// <param name="stocks">The list of stocks that is going to be checked.</param>
        /// <returns></returns>
        public static bool StockIsPresent(Stock stock, List<Stock> stocks)
        {
            if(stocks == null) return false;// if the list is empty no need to continue
           

            foreach (Stock s in stocks)
            {
                //Checks by name of observer
                if (s.Name.Equals(stock.Name))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
