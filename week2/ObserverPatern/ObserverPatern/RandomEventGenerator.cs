﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace ObserverPatern
{
    public class RandomEventGenerator
    {
        IStockObserverPush[] customOBS;//custom list of observers
        Stock[] customStocks = new Stock[] { new Stock("NASDAQ", 359.12), new Stock("AAPL", 10), new Stock("MSFT", 312) }; //custom stocks
        
        Thread t; //thread to work async with the system
        private static int sleepTime = 2000;
        IServer server;
        private static Random r = new Random();
        /// <summary>
        /// Constructor for a new random generator for the server
        /// </summary>
        /// <param name="server">The server with which it will communicate</param>
        public RandomEventGenerator(IServer server)
        {
            this.server = server;
            //fills the custom observer list
            customOBS = new IStockObserverPush[] { new StockObserverPush(server, "Josephi", null), new StockObserverPush(server, "Lopez", null), new StockObserverPush(server, "Misho", null) };
            foreach (IStockObserverPush obs in customOBS)
            {
                //attaches them to the list of listeners
                server.Attach(obs);
            }
            //initalizes the thread
            t = new Thread(new ThreadStart(HandleTimer));
            t.Start();
        }

        /// <summary>
        /// Executes the random event tasks
        /// </summary>
        private void HandleTimer()
        {
            while(true)
            {
                Thread.Sleep(sleepTime);
                int randomInt = r.Next(0, 11);
                switch (randomInt)
                {
                    case 2:

                        SellFromRandomStock();
                        break;
                    case 4:
                        BuyFromRandomStock();
                        break;
                    case 6:
                        SellFromRandomStock();
                        break;
                    case 8:
                        ChangeRandomPriceFromRandomStock();
                        break;
                    case 10:
                        ChangeRandomPriceFromRandomStock();
                        break;
                    default:
                        sleepTime = 1000;
                        break;
                }
               
            }
          
           
          
        }


        /// <summary>
        /// Sells a random stock from the custom stocks and current stocks.
        /// </summary>
        private void SellFromRandomStock()
        {

            Stock[] stocksToUse = PickStocks();

            int randomIndex = r.Next(0, stocksToUse.Length);
            Stock o = stocksToUse[randomIndex];
            
            randomIndex = r.Next(0, customOBS.Length);
            IStockObserverPush byWho = customOBS[randomIndex];
            int randomQuantity = r.Next(1, 10);
            o.Quantity = randomQuantity;
            byWho.Sell(o, byWho);
            
        }

        /// <summary>
        /// Picks a random betwen the custom stocks or the current stocks
        /// </summary>
        /// <returns>The picked list</returns>

        private Stock[] PickStocks()
        {
            Stock[] stocksToUse;
            int listPicker = r.Next(1, 5);
            if (listPicker > 2)
                stocksToUse = customStocks;
            else
            {
                stocksToUse = server.GetStock().ToArray();
            }
            return stocksToUse;
        }

        /// <summary>
        /// Buys a random ammount from an already registered stock
        /// </summary>
        private void BuyFromRandomStock()
        {
         

            int randomIndex = r.Next(0, server.GetStock().Count);
            Stock o = server.GetStock().ElementAt(randomIndex);

            if (o.Quantity == 0) return; //if there is no quantity the stock can not be bought
            randomIndex = r.Next(0, customOBS.Length);
            IStockObserverPush byWho = customOBS[randomIndex];
            int randomQuantity = r.Next(1, o.Quantity);
            o.ReduceQuantityBy(randomQuantity);//buy random quantity
            byWho.Buy(o, byWho);

        }

        /// <summary>
        /// Changes the price of a random stock with a difference between +- 0..2
        /// </summary>
        private void ChangeRandomPriceFromRandomStock()
        {
            List<Stock> serverStock = server.GetStock();
            if (serverStock.Count > 0)
            {
                

                int randomIndex = r.Next(0, serverStock.Count);
                Stock o = serverStock[randomIndex];
                
                double oldPrice = o.Price;
                double newPrice = r.Next((int)oldPrice, (int)oldPrice + 2);
                double plusMinus = r.Next(0, 3) < 1 ? -1 : 1;
                newPrice += (r.NextDouble() * plusMinus);
                (server as StockServer).SetStock(o, newPrice);//Sends the transaction to the server
                                              
            }
            else
            {
                sleepTime = 4000;
            }
        }

        /// <summary>
        /// Stops the thread of the random generator ( control of thread )
        /// </summary>
        public void StopThread()
        {
            t.Abort();
            
        }
    }
}
