﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObserverPatern
{
    public partial class Form1 : Form
    {


        private IStockObserverPush pushObserver;
        private IStockObserverPull pullObserver;
        private IServer server;
        static bool adminViewIsOpen = false;
        private String observerName;
        private bool isPushInit;

        public Form1()
        {
            InitializeComponent();
            liBoxPull.Columns.Add("Stock");
            liBoxPull.Columns.Add("Price");
            liBoxPull.Columns.Add("Change");
            liBoxPull.Columns.Add("Quantity");
            liBoxPull.View = View.Details;
            SizeLastColumn(liBoxPull);
            liBoxStock.Columns.Add("Stock");
            liBoxStock.Columns.Add("Price");
            liBoxStock.Columns.Add("Change");
            liBoxStock.Columns.Add("Quantity");
            liBoxStock.View = View.Details;
            if (pullObserver == null)
            {
                butUpdate.Enabled = false;
            }



        }

        public Form1(String name, IServer server, bool isPushUser) : base()
        {
            InitializeComponent();

            this.observerName = name;
            this.server = server;
            isPushInit = isPushUser;
            this.Text = String.Format("Stock observer {0}", observerName);

        }


        private void Form1_Load(object sender, EventArgs e)
        {

            liBoxPull.Columns.Add("Stock");
            liBoxPull.Columns.Add("Price");
            liBoxPull.Columns.Add("Change");
            liBoxPull.Columns.Add("Quantity");
            liBoxPull.View = View.Details;
            SizeLastColumn(liBoxPull);
            liBoxStock.Columns.Add("Stock");
            liBoxStock.Columns.Add("Price");
            liBoxStock.Columns.Add("Change");
            liBoxStock.Columns.Add("Quantity");
            liBoxStock.View = View.Details;
            if (pullObserver == null)
            {
                butUpdate.Enabled = false;
            }
            if (isPushInit) UpdateUser("Push");

        }

        private void InitializeTextBox()
        {

            AutoCompleteStringCollection allowedStatorTypes = new AutoCompleteStringCollection();

            List<String> allStockType = new List<string>();
            foreach (ListViewItem item in liBoxStock.Items)
            {
                allStockType.Add(item.Text);
            }
            if (allStockType != null && allStockType.Count > 0)
            {
                foreach (string item in allStockType)
                {
                    allowedStatorTypes.Add(item);
                }
            }

            txtStockType.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtStockType.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtStockType.AutoCompleteCustomSource = allowedStatorTypes;
        }

        private void SizeLastColumn(ListView lv)
        {
            lv.Columns[lv.Columns.Count - 1].Width = -2;
        }

        /// <summary>
        /// Used to update the admin view when the stock market has a new update. //Like a callback service, but in an offline version
        /// </summary>
        private void RegisterChange()
        {

            if (adminViewIsOpen)
                AdminView.UpdateServer(server);
        }

        private void UpdatePushObserverInterface(List<Stock> stocks)
        {
            GenerateListView(liBoxStock, stocks);
        }

        private void GenerateListView(ListView libBox, List<Stock> stocks)
        {
            List<ListViewItem> lastCollection = new List<ListViewItem>();
            this.Invoke(new Action(() =>
            {
                foreach (ListViewItem item in libBox.Items)
                {
                    lastCollection.Add(item);
                }
                libBox.Items.Clear();
            })
            );

            foreach (Stock s in stocks)
            {


                ListViewItem lvi = new ListViewItem();
                lvi.Text = s.Name;
                bool toBeBold = true;
                foreach (ListViewItem item in lastCollection)
                {
                    if (item.Text == s.Name)
                    {
                        double oldPrice = Double.Parse(item.SubItems[1].Text);

                        if (oldPrice != Double.Parse(s.Price.ToString("0.00"))) break;

                        int oldQuantity = int.Parse(item.SubItems[3].Text);
                        if (oldQuantity != s.Quantity) break;
                        toBeBold = false;
                        break;
                    }
                }
                if (toBeBold) lvi.Font = new Font(lvi.Font, FontStyle.Bold);
                lvi.SubItems.Add(s.Price.ToString("0.00"));
                lvi.SubItems.Add(s.GetChange());
                lvi.SubItems.Add(s.Quantity.ToString());

                this.Invoke(new Action(() => libBox.Items.Add(lvi)));
            }
            this.Invoke(new Action(() =>
            {
                libBox.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                libBox.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }));
            this.Invoke(new Action(InitializeTextBox));
        }

        private void UpdatePullObserverInterface(List<Stock> stocks)
        {
            GenerateListView(liBoxPull, stocks);
        }

        public static void AdminViewClosed()
        {
            adminViewIsOpen = false;
        }

        private void butBuy_Click(object sender, EventArgs e)
        {
            Stock r = null;

            pushObserver.Buy(r, pushObserver);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Only because we are simulating the server 
            if (pullObserver != null)
                (pullObserver as StockObserverPull).ClosedForm();
            if (pushObserver != null)
            {
                (pushObserver as StockObserverPush).ClosedForm();
                //server.Detach(pushObserver);
            }
        }

        private void butUpdate_Click(object sender, EventArgs e)
        {
            //for pull observer
            pullObserver.Update(server);
        }

        private void btnSub_Click(object sender, EventArgs e)
        {

            MessageBox mbox = new MessageBox(this);
            mbox.ShowDialog();

        }

        private void liBoxStock_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void liBoxPull_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void UpdateUser(String input)
        {
            Font oldFont, font;
            switch (input)
            {
                case "Pull":
                    if (pullObserver != null) return;
                    pullObserver = new StockObserverPull(server, observerName, this);
                    server.Detach(pushObserver);
                    pushObserver = null;
                    butUpdate.Enabled = true;
                    oldFont = labPull.Font;
                    labPush.Font = oldFont;
                    font = new Font(labPush.Font.FontFamily, 15);
                    labPull.Font = new Font(font, FontStyle.Bold);


                    break;
                case "Push":
                    if (pushObserver != null) return;
                    pullObserver = null;
                    pushObserver = new StockObserverPush(server, observerName, this);
                    server.Attach(pushObserver);
                    butUpdate.Enabled = false;

                    oldFont = labPush.Font;
                    labPull.Font = oldFont;
                    font = new Font(labPush.Font.FontFamily, 15);
                    labPush.Font = new Font(font, FontStyle.Bold);
                    break;
            }

        }

        private void buttonSell_Click(object sender, EventArgs e)
        {
            Stock s = ValidateStock();
            if (pullObserver == null && pushObserver == null) return;
            if(pullObserver == null)
            {
                pushObserver.Sell(s, pushObserver);
            }
            else
            {
                pullObserver.Sell(s, pullObserver);
            }
        }

        private void buttonBuy_Click(object sender, EventArgs e)
        {
            Stock s = ValidateStock();
            if (pullObserver == null && pushObserver == null) return;
            if (pullObserver == null)
            {
                pushObserver.Buy(s, pushObserver);
            }
            else
            {
                pullObserver.Buy(s, pullObserver);
            }
        }

        private Stock ValidateStock()
        {
            double price;
            int quantity;
            if (!Double.TryParse(txtPrice.Text, out price))
            {
                price = 0;
            }
            if (!int.TryParse(txtPrice.Text, out quantity))
            {
                quantity = 0;
            }
            Stock s = new Stock(txtStockType.Text, price);
            s.Quantity = int.Parse(txtQuantity.Text);
            return s;
        }
    }
}
