﻿using System;
using System.Collections.Generic;

namespace ObserverPatern
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labPush = new System.Windows.Forms.Label();
            this.labPull = new System.Windows.Forms.Label();
            this.butUpdate = new System.Windows.Forms.Button();
            this.btnSub = new System.Windows.Forms.Button();
            this.liBoxStock = new System.Windows.Forms.ListView();
            this.liBoxPull = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPrice = new System.Windows.Forms.NumericUpDown();
            this.txtQuantity = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStockType = new System.Windows.Forms.TextBox();
            this.buttonBuy = new System.Windows.Forms.Button();
            this.buttonSell = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // labPush
            // 
            this.labPush.AutoSize = true;
            this.labPush.Location = new System.Drawing.Point(90, 16);
            this.labPush.Name = "labPush";
            this.labPush.Size = new System.Drawing.Size(114, 13);
            this.labPush.TabIndex = 8;
            this.labPush.Text = "Stock Observer (Push)";
            // 
            // labPull
            // 
            this.labPull.AutoSize = true;
            this.labPull.Location = new System.Drawing.Point(538, 16);
            this.labPull.Name = "labPull";
            this.labPull.Size = new System.Drawing.Size(107, 13);
            this.labPull.TabIndex = 9;
            this.labPull.Text = "Stock Observer (Pull)";
            // 
            // butUpdate
            // 
            this.butUpdate.Location = new System.Drawing.Point(669, 377);
            this.butUpdate.Name = "butUpdate";
            this.butUpdate.Size = new System.Drawing.Size(105, 56);
            this.butUpdate.TabIndex = 11;
            this.butUpdate.Text = "Update";
            this.butUpdate.UseVisualStyleBackColor = true;
            this.butUpdate.Click += new System.EventHandler(this.butUpdate_Click);
            // 
            // btnSub
            // 
            this.btnSub.Location = new System.Drawing.Point(341, 2);
            this.btnSub.Name = "btnSub";
            this.btnSub.Size = new System.Drawing.Size(101, 41);
            this.btnSub.TabIndex = 12;
            this.btnSub.Text = "Switch mode";
            this.btnSub.UseVisualStyleBackColor = true;
            this.btnSub.Click += new System.EventHandler(this.btnSub_Click);
            // 
            // liBoxStock
            // 
            this.liBoxStock.AutoArrange = false;
            this.liBoxStock.GridLines = true;
            this.liBoxStock.Location = new System.Drawing.Point(18, 59);
            this.liBoxStock.Name = "liBoxStock";
            this.liBoxStock.Size = new System.Drawing.Size(350, 312);
            this.liBoxStock.TabIndex = 4;
            this.liBoxStock.TabStop = false;
            this.liBoxStock.UseCompatibleStateImageBehavior = false;
            this.liBoxStock.SelectedIndexChanged += new System.EventHandler(this.liBoxStock_SelectedIndexChanged);
            // 
            // liBoxPull
            // 
            this.liBoxPull.GridLines = true;
            this.liBoxPull.Location = new System.Drawing.Point(424, 59);
            this.liBoxPull.Name = "liBoxPull";
            this.liBoxPull.Size = new System.Drawing.Size(350, 312);
            this.liBoxPull.TabIndex = 14;
            this.liBoxPull.UseCompatibleStateImageBehavior = false;
            this.liBoxPull.SelectedIndexChanged += new System.EventHandler(this.liBoxPull_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPrice);
            this.groupBox1.Controls.Add(this.txtQuantity);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtStockType);
            this.groupBox1.Controls.Add(this.buttonBuy);
            this.groupBox1.Controls.Add(this.buttonSell);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(18, 400);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(556, 130);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buy or sell a stock";
            // 
            // txtPrice
            // 
            this.txtPrice.DecimalPlaces = 2;
            this.txtPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.txtPrice.Location = new System.Drawing.Point(288, 101);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(74, 24);
            this.txtPrice.TabIndex = 23;
            // 
            // txtQuantity
            // 
            this.txtQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.txtQuantity.Location = new System.Drawing.Point(288, 67);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(74, 24);
            this.txtQuantity.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(215, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 24);
            this.label3.TabIndex = 21;
            this.label3.Text = "Price";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(196, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 24);
            this.label2.TabIndex = 19;
            this.label2.Text = "Quantity";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 24);
            this.label1.TabIndex = 17;
            this.label1.Text = "Stock";
            // 
            // txtStockType
            // 
            this.txtStockType.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.txtStockType.Location = new System.Drawing.Point(90, 62);
            this.txtStockType.Name = "txtStockType";
            this.txtStockType.Size = new System.Drawing.Size(100, 29);
            this.txtStockType.TabIndex = 16;
            // 
            // buttonBuy
            // 
            this.buttonBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBuy.Location = new System.Drawing.Point(437, 89);
            this.buttonBuy.Name = "buttonBuy";
            this.buttonBuy.Size = new System.Drawing.Size(113, 36);
            this.buttonBuy.TabIndex = 1;
            this.buttonBuy.Text = "Buy";
            this.buttonBuy.UseVisualStyleBackColor = true;
            this.buttonBuy.Click += new System.EventHandler(this.buttonBuy_Click);
            // 
            // buttonSell
            // 
            this.buttonSell.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSell.Location = new System.Drawing.Point(437, 26);
            this.buttonSell.Name = "buttonSell";
            this.buttonSell.Size = new System.Drawing.Size(113, 36);
            this.buttonSell.TabIndex = 0;
            this.buttonSell.Text = "Sell";
            this.buttonSell.UseVisualStyleBackColor = true;
            this.buttonSell.Click += new System.EventHandler(this.buttonSell_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 564);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.liBoxPull);
            this.Controls.Add(this.liBoxStock);
            this.Controls.Add(this.btnSub);
            this.Controls.Add(this.butUpdate);
            this.Controls.Add(this.labPull);
            this.Controls.Add(this.labPush);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        internal void UpdateInterface(List<Stock> stocks)
        {
            if (pullObserver == null)
                UpdatePushObserverInterface(stocks);
            if (pushObserver == null)
                UpdatePullObserverInterface(stocks);
        }

        #endregion
        private System.Windows.Forms.Label labPush;
        private System.Windows.Forms.Label labPull;
        private System.Windows.Forms.Button butUpdate;
        private System.Windows.Forms.Button btnSub;
        private System.Windows.Forms.ListView liBoxStock;
        private System.Windows.Forms.ListView liBoxPull;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonBuy;
        private System.Windows.Forms.Button buttonSell;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStockType;
        private System.Windows.Forms.NumericUpDown txtPrice;
        private System.Windows.Forms.NumericUpDown txtQuantity;
    }
}

