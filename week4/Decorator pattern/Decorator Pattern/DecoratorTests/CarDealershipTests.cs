﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Decorator_Pattern;

namespace DecoratorTests
{
    [TestClass]
    public class CarDealershipTests
    {
        [TestMethod]
        public void CreateCarDealership()
        {
            CarDealership cd = new CarDealership();

            Assert.IsNotNull(cd);
        }

        [TestMethod]
        public void GetCarDealershipCar()
        {
            CarDealership cd = new CarDealership();

            ICar car = cd.BMWF10;
            Assert.IsNotNull(car);
            String expectedModel = "\n Model: BMW F10";
            Assert.AreEqual(expectedModel, car.getDescription());
        }

        [TestMethod]
        public void GetCarAddExtra()
        {
            CarDealership cd = new CarDealership();

            ICar car = cd.BMWF10;
            Assert.IsNotNull(car);
            String expectedModel = "\n Model: BMW F10";
            Assert.AreEqual(expectedModel, car.getDescription());
            car = cd.AddExtra(car, Form1.PossibleExtra.AirCo);

            String expectedModelAndExtra = "Air condition, \n Model: BMW F10";
            Assert.AreEqual(expectedModelAndExtra, car.getDescription());

        }

        [TestMethod]
        public void GetCarAddExtraExtra()
        {
            CarDealership cd = new CarDealership();

            ICar car = cd.BMWF10;
            Assert.IsNotNull(car);
            car = cd.AddExtra(car, Form1.PossibleExtra.AirCo);

            String expectedModelAndExtra = "Air condition, \n Model: BMW F10";
            Assert.AreEqual(expectedModelAndExtra, car.getDescription());

            car = cd.AddExtra(car, Form1.PossibleExtra.AirCo);

            expectedModelAndExtra = "Air condition, Air condition, \n Model: BMW F10";
            Assert.AreEqual(expectedModelAndExtra, car.getDescription());

            car = cd.AddExtra(car, Form1.PossibleExtra.Heating);

            expectedModelAndExtra = "Heating, Air condition, Air condition, \n Model: BMW F10";
            Assert.AreEqual(expectedModelAndExtra, car.getDescription());
            
        }

        [TestMethod]
        public void GetPrice()
        {
            CarDealership cd = new CarDealership();

            ICar car = cd.BMWF10;
            Assert.AreEqual(60000, car.getPrice());
            car = cd.AddExtra(car, Form1.PossibleExtra.AirCo);

            Assert.AreEqual(63500, car.getPrice());

            car = cd.AddExtra(car, Form1.PossibleExtra.AirCo);

            Assert.AreEqual(67000, car.getPrice());

            car = cd.AddExtra(car, Form1.PossibleExtra.Heating);

            Assert.AreEqual(71000, car.getPrice());

        }

        [TestMethod]
        public void GetPriceRemoved()
        {
            CarDealership cd = new CarDealership();
            //increase
            ICar car = cd.BMWF10;
            Assert.AreEqual(60000, car.getPrice());
            car = cd.AddExtra(car, Form1.PossibleExtra.AirCo);

            Assert.AreEqual(63500, car.getPrice());

            car = cd.AddExtra(car, Form1.PossibleExtra.AirCo);

            Assert.AreEqual(67000, car.getPrice());

            car = cd.AddExtra(car, Form1.PossibleExtra.Heating);

            Assert.AreEqual(71000, car.getPrice());
            //Stop increase


            //Start remove
            car = cd.RemoveExtra(car);

            Assert.AreEqual(67000, car.getPrice());

            car = cd.RemoveExtra(car);

            Assert.AreEqual(63500, car.getPrice());


            car = cd.RemoveExtra(car);
            //Car is left to base
            Assert.AreEqual(60000, car.getPrice());

              car = cd.RemoveExtra(car);
            //Nothing more to remove
            Assert.AreEqual(60000, car.getPrice());
            
        }
    }
}

