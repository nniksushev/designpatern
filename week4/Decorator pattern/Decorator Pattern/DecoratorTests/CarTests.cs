﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Decorator_Pattern;

namespace DecoratorTests
{
    [TestClass]
    public class CarTests
    {
        [TestMethod]
        public void CreateCarBMWF10()
        {
            ICar car = new BMWF10();
            String expectedModel = "\n Model: BMW F10";
            Assert.AreEqual(expectedModel, car.getDescription());
        }

        [TestMethod]
        public void CreateCarBMWFM5()
        {
            ICar car = new BMWM5();
            String expectedModel = "\n Model: BMW M5";
            Assert.AreEqual(expectedModel, car.getDescription());
        }

        [TestMethod]
        public void CreateCarBMWFX5()
        {
            ICar car = new BMWX5();
            String expectedModel = "\n Model: BMW X5";
            Assert.AreEqual(expectedModel, car.getDescription());
            
        }


        [TestMethod]
        public void CarBMWF10GetPrice()
        {
            ICar car = new BMWF10();

            Assert.AreEqual(60000, car.getPrice());
        }

        [TestMethod]
        public void CarBMWM5GetPrice()
        {
            ICar car = new BMWM5();
            Assert.AreEqual(40000, car.getPrice());
        }

        [TestMethod]
        public void CarBMWX5GetPrice()
        {
            ICar car = new BMWX5();
            Assert.AreEqual(50000, car.getPrice());

        }
    }
}
