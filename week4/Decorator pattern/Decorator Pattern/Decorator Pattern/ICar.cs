﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern
{
    /// <summary>
    /// Interface for Decorator Patern to link cars and extras for cars
    /// </summary>
    public interface ICar
    {
        /// <summary>
        /// Returns the price of the the car. If it has extras the price is also calculated
        /// </summary>
        /// <returns>Double. Total price of the car</returns>
        double getPrice();
        /// <summary>
        /// Gets the description of the car.
        /// Format: Extra + Extra + Extra + Car ( where Extra is for each extra it has ) and Car is the Base
        /// </summary>
        /// <returns>The description of the car</returns>
        string getDescription();

        /// <summary>
        /// Returns the previous state of the car, before adding the last extra.
        /// If no extra is possible to be added returns the previous from which the extra started.
        /// If there is no other extra returns the Car base.
        /// </summary>
        /// <returns>ICar object of the previous car state.</returns>
        ICar GetPrevious();
    }
}
