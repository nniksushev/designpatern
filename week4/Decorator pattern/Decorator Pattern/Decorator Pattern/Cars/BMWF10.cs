﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern
{
    public class BMWF10 : ICar
    {
        string description = "BMW F10";
        double price = 60000;
        public string getDescription()
        {
            return "\n Model: " + description;
        }
        public ICar GetPrevious()
        {
            return this;
        }
        public double getPrice()
        {
            return price;
        }

        
    }
}
