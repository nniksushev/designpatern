﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern
{
    public class BMWX5 : ICar
    {
        string description = "BMW X5";
        double price = 50000;
        public string getDescription()
        {
            return "\n Model: " + description;
        }

        public double getPrice()
        {
            return price;
        }

        public ICar GetPrevious()
        {
            return this;
        }
    }
}
