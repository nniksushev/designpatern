﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern
{
   public class BMWM5 : ICar
    {
        string description = "BMW M5";
        double price = 40000;
        public string getDescription()
        {
            return "\n Model: " + description;
        }

        public double getPrice()
        {
            return price;
        }

        public ICar GetPrevious()
        {
            return this;
        }
    }
}
