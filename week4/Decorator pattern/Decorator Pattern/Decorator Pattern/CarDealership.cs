﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator_Pattern.Extras;

namespace Decorator_Pattern

{
    /// <summary>
    /// Car dealership class containing car models to be used as bases
    /// Also contains methods for adding and removing extras to a car
    /// </summary>
    public class CarDealership
    {
        ICar bmwf10, bmwm5, bmwx5; //models of cars
                     
        /// <summary>
        /// Initializes the car base models.
        /// </summary>
        public CarDealership()
        {
            bmwf10 = new BMWF10();
            bmwm5 = new BMWM5();
            bmwx5 = new BMWX5();
        }

        /// <summary>
        /// Returns a BMWF10 object
        /// </summary>
        public ICar BMWF10 { get { return bmwf10; } }
        /// <summary>
        /// Returns a BMWM5 object
        /// </summary>
        public ICar BMWM5 { get { return bmwm5; } }
        /// <summary>
        /// Returns a BMWX5 object
        /// </summary>
        public ICar BMWX5 { get { return bmwx5; } }

        /// <summary>
        /// Extending a current car with additional extra
        /// </summary>
        /// <param name="toCar">To which car base</param>
        /// <param name="extra">The type of the extra to be added</param>
        /// <returns>A new Icar object with the car + extra</returns>
        public ICar AddExtra( ICar toCar, Form1.PossibleExtra extra)
        {
            if (extra == Form1.PossibleExtra.AirCo)
            {
                return new AirCo(toCar);
            }
            else if (extra == Form1.PossibleExtra.Heating)
            {
                return new Heating(toCar);
            }
            else if (extra == Form1.PossibleExtra.Rims)
            {
                return new Rims(toCar);
            }
            else
            {
              return new Wood(toCar);
            }
        }

        /// <summary>
        /// Removes an Extra from a car, if no extra is possible to be removed returns the base.
        /// </summary>
        /// <param name="car">The car to remove an extra from</param>
        /// <returns>Icar object, the base of the extra</returns>
        public ICar RemoveExtra(ICar car)
        {
            return car.GetPrevious();
        }
    }
}
