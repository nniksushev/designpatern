﻿using Decorator_Pattern.Extras;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decorator_Pattern
{
    public partial class Form1 : Form
    {
        private CarDealership carDealr;
        public Form1()
        {
            InitializeComponent();
            carDealr = new CarDealership();

            //View initializing
            listView1.Columns.Add("Car details");
            listView1.Columns.Add("Price");
            listView1.View = View.Details;
            SizeLastColumn(listView1);

            //Possible extra combo box initialization
            comboBox1.Items.Add(PossibleExtra.AirCo);
            comboBox1.Items.Add(PossibleExtra.Heating);
            comboBox1.Items.Add(PossibleExtra.Rims);
            comboBox1.Items.Add(PossibleExtra.Wood);
            comboBox1.SelectedIndex = 0;

            //Possible cars combo box initialization
            comboBox2.Items.Add(PossibleCar.BMWMF10);
            comboBox2.Items.Add(PossibleCar.BMWX5);
            comboBox2.Items.Add(PossibleCar.BMWM5);
            comboBox2.SelectedIndex = 0;
        }


        /// <summary>
        /// Resize last column size
        /// </summary>
        /// <param name="lv">The listview to resize</param>
        private void SizeLastColumn(ListView lv)
        {
            lv.Columns[lv.Columns.Count - 1].Width = -2;
        }

        /// <summary>
        /// Update the listview with the new car elements.
        /// </summary>
        private void UpdateList()
        {
            foreach (ListViewItem item in listView1.Items)
            {
                //puts the font to regular
                item.Font = new Font(item.Font, FontStyle.Regular); // makes the old values to normal
                //gets the old data of the car, in case there has been a change
                ICar tempCar = (ICar)item.Tag;
                item.Text = tempCar.getDescription();
                item.SubItems[1].Text = "$" + tempCar.getPrice().ToString();
            }

            this.Invoke(new Action(() =>
            {
                //Resize based on content
                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            ));

        }

        private void FillCarToView(ICar car)
        {
            UpdateList();

            ListViewItem lvi = new ListViewItem();
            lvi.Tag = car; //Sets the item tag to the car value, used in future changes.
            lvi.Text = car.getDescription();// car description;
            lvi.Font = new Font(lvi.Font, FontStyle.Bold); // makes the new value to bold
            ListViewItem.ListViewSubItem subItem = new ListViewItem.ListViewSubItem();
            subItem.Text = "$" + car.getPrice().ToString();
            lvi.SubItems.Add(subItem);

            this.Invoke(new Action(() => listView1.Items.Add(lvi)));
            this.Invoke(new Action(() =>
            {
                //Resize based on content
                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            ));


        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Adds a new car from the car delaership base.
            if ((PossibleCar)comboBox2.SelectedItem == PossibleCar.BMWMF10)
                FillCarToView(carDealr.BMWF10);
            if ((PossibleCar)comboBox2.SelectedItem == PossibleCar.BMWM5)
                FillCarToView(carDealr.BMWM5);
            if ((PossibleCar)comboBox2.SelectedItem == PossibleCar.BMWX5)
                FillCarToView(carDealr.BMWX5);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count != 0) // interactive is selected -> allow to add or remove extra
            {
                butAddNew.Enabled = true;
                butRemoveLast.Enabled = true;
            }
            else
            {
                butRemoveLast.Enabled = false;
                butAddNew.Enabled = false;

            }
            UpdateList();
        }

        private void butAddNew_Click(object sender, EventArgs e)
        {
            //Loads the old car from the list from the tag
            ICar car = (ICar)listView1.Items[listView1.SelectedIndices[0]].Tag;

            //based on the selected item adds a new part
            car = carDealr.AddExtra(car, (PossibleExtra)comboBox1.SelectedItem);
            listView1.Items[listView1.SelectedIndices[0]].Tag = car; //updates the tag 
            UpdateList();
        }


        public enum PossibleExtra
        {
            AirCo, Heating, Rims, Wood
        }

        enum PossibleCar
        {
            BMWMF10, BMWM5, BMWX5
        }

        private void butRemoveLast_Click(object sender, EventArgs e)
        {
            //Gets the items tag from the reference
            ICar car = (ICar)listView1.Items[listView1.SelectedIndices[0]].Tag;
            car = carDealr.RemoveExtra(car);
            //Updates the tag based on the new car element
            listView1.Items[listView1.SelectedIndices[0]].Tag = car;
            UpdateList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
