﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern
{
    /// <summary>
    /// Accesories class for cars. Abstract
    /// </summary>
    public abstract class Extra : ICar
    {
        protected ICar car; //The car the extra belong to
        protected string description;
        protected double price;
        /// <summary>
        /// Constructor for extra using the car as a base to add to.
        /// </summary>
        /// <param name="_car">The car to be used as a base.</param>
        protected Extra(ICar _car)
        {
            car = _car;
        }           
        /// <summary>
        /// Returns a string with the cars description 
        /// format : description + car description
        /// </summary>
        /// <returns>String with extra and car details</returns>
        public string getDescription()
        {
            return description + ", " + car.getDescription();
        }
        /// <summary>
        /// Returns the price of the extra + the price of the car
        /// </summary>
        /// <returns>Double. Price extra + car price</returns>
        public double getPrice()
        {
            return price + car.getPrice();
        }
        /// <summary>
        /// Returns the previous state of the car, to which the extra is attached to.
        /// </summary>
        /// <returns>Icar object, the car base</returns>
        public ICar GetPrevious()
        {
            return this.car;
        }

       
    }
}
