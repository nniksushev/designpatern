﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern.Extras
{
    public class Heating : Extra
    {
        public Heating(ICar _car) : base(_car)
        {
            description = "Heating";
            price = 4000;
        }
    }
}
