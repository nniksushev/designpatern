﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern.Extras
{
    class Rims : Extra
    {
        public Rims(ICar _car) : base(_car)
        {
            description = "Rims";
            price = 4500;
        }
    }
}
