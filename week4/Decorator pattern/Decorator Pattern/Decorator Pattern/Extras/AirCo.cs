﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern.Extras
{
    public class AirCo : Extra
    {
        
        public AirCo(ICar _car) :base(_car)
        {
            description = "Air condition";
            price = 3500;
        }

    }
}
