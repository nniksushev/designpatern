﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pattern.Extras
{
    class Wood : Extra
    {
        public Wood(ICar _car) : base(_car)
        {
            description = "Wooden Interior";
            price = 5600;
        }
    }
}
