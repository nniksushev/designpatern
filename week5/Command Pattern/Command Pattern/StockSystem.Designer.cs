﻿namespace Command_Pattern
{
    partial class StockSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnBuy = new System.Windows.Forms.Button();
            this.btnSell = new System.Windows.Forms.Button();
            this.butPlace = new System.Windows.Forms.Button();
            this.listExecuted = new System.Windows.Forms.ListView();
            this.listToBeApproved = new System.Windows.Forms.ListView();
            this.listStockView = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.butClear = new System.Windows.Forms.Button();
            this.numericToChange = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericToChange)).BeginInit();
            this.SuspendLayout();
            // 
            // btnUndo
            // 
            this.btnUndo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUndo.Location = new System.Drawing.Point(345, 510);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(110, 64);
            this.btnUndo.TabIndex = 2;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnBuy
            // 
            this.btnBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuy.Location = new System.Drawing.Point(30, 162);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(75, 42);
            this.btnBuy.TabIndex = 3;
            this.btnBuy.Text = "Buy";
            this.btnBuy.UseVisualStyleBackColor = true;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // btnSell
            // 
            this.btnSell.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSell.Location = new System.Drawing.Point(300, 162);
            this.btnSell.Name = "btnSell";
            this.btnSell.Size = new System.Drawing.Size(75, 42);
            this.btnSell.TabIndex = 4;
            this.btnSell.Text = "Sell";
            this.btnSell.UseVisualStyleBackColor = true;
            this.btnSell.Click += new System.EventHandler(this.btnSell_Click);
            // 
            // butPlace
            // 
            this.butPlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butPlace.Location = new System.Drawing.Point(342, 324);
            this.butPlace.Name = "butPlace";
            this.butPlace.Size = new System.Drawing.Size(110, 77);
            this.butPlace.TabIndex = 5;
            this.butPlace.Text = "<-- Place Orders <--";
            this.butPlace.UseVisualStyleBackColor = true;
            this.butPlace.Click += new System.EventHandler(this.butPlace_Click);
            // 
            // listExecuted
            // 
            this.listExecuted.Location = new System.Drawing.Point(27, 324);
            this.listExecuted.Name = "listExecuted";
            this.listExecuted.Size = new System.Drawing.Size(309, 250);
            this.listExecuted.TabIndex = 6;
            this.listExecuted.UseCompatibleStateImageBehavior = false;
            // 
            // listToBeApproved
            // 
            this.listToBeApproved.Location = new System.Drawing.Point(461, 324);
            this.listToBeApproved.Name = "listToBeApproved";
            this.listToBeApproved.Size = new System.Drawing.Size(309, 250);
            this.listToBeApproved.TabIndex = 7;
            this.listToBeApproved.UseCompatibleStateImageBehavior = false;
            // 
            // listStockView
            // 
            this.listStockView.HideSelection = false;
            this.listStockView.Location = new System.Drawing.Point(27, 45);
            this.listStockView.MultiSelect = false;
            this.listStockView.Name = "listStockView";
            this.listStockView.Size = new System.Drawing.Size(348, 111);
            this.listStockView.TabIndex = 8;
            this.listStockView.UseCompatibleStateImageBehavior = false;
            this.listStockView.SelectedIndexChanged += new System.EventHandler(this.listStockView_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Stock overview";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(49, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Executed transactions";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(476, 297);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Orders waiting for approval";
            // 
            // butClear
            // 
            this.butClear.Location = new System.Drawing.Point(690, 288);
            this.butClear.Name = "butClear";
            this.butClear.Size = new System.Drawing.Size(80, 35);
            this.butClear.TabIndex = 12;
            this.butClear.Text = "Clear orders";
            this.butClear.UseVisualStyleBackColor = true;
            this.butClear.Click += new System.EventHandler(this.butClear_Click);
            // 
            // numericToChange
            // 
            this.numericToChange.Location = new System.Drawing.Point(189, 170);
            this.numericToChange.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numericToChange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericToChange.Name = "numericToChange";
            this.numericToChange.Size = new System.Drawing.Size(59, 20);
            this.numericToChange.TabIndex = 13;
            this.numericToChange.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(126, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "How many";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Millimeter, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(454, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(254, 131);
            this.label5.TabIndex = 15;
            this.label5.Text = "Select a stock to buy or sell from";
            // 
            // StockSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 586);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericToChange);
            this.Controls.Add(this.butClear);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listStockView);
            this.Controls.Add(this.listToBeApproved);
            this.Controls.Add(this.listExecuted);
            this.Controls.Add(this.butPlace);
            this.Controls.Add(this.btnSell);
            this.Controls.Add(this.btnBuy);
            this.Controls.Add(this.btnUndo);
            this.Name = "StockSystem";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.StockSystem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericToChange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnSell;
        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.Button butPlace;
        private System.Windows.Forms.ListView listExecuted;
        private System.Windows.Forms.ListView listToBeApproved;
        private System.Windows.Forms.ListView listStockView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button butClear;
        private System.Windows.Forms.NumericUpDown numericToChange;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

