﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Pattern
{
    /// <summary>
    /// The request class
    /// </summary>
    public class Stock
    {
        private string name;
        private int quantity;
        private double price;


        /// <summary>
        /// Buy a specific ammount from a stock
        /// </summary>
        /// <param name="ammount">the ammount to buy</param>
        public void Buy(int ammount)
        {
            quantity += ammount;
            
        }
        /// <summary>
        /// Sell a specific ammount from a stock
        /// </summary>
        /// <param name="ammount">The ammount to sell</param>
        public void Sell(int ammount)
        {
            if (quantity >= ammount)
            {
                quantity -= ammount;
            }
            else
            {
               throw new InvalidOperationException("Insufficient quantity for selling stock: " + name); //You can not sell because there is not enough
            }

        }

        /// <summary>
        /// Returns a string of an order in format Stock [ Name: {name} , Quantity: {quantity} Price: {price} ]
        /// </summary>
        /// <returns>String formated order</returns>
        public override string ToString()
        {
            return String.Format("Stock [ Name: " + name + ", Quantity: " + quantity.ToString() + " Price: " + price .ToString()
                     + " ]");
        }

       /// <summary>
       /// Constructor for stock
       /// </summary>
       /// <param name="name">The name of the stock</param>
       /// <param name="quantity">The ammount of the stock</param>
       /// <param name="price">The price of a stock</param>
        public Stock(string name, int quantity, double price)
        {
            this.name = name;
            this.quantity = quantity;
            this.price = price;
        }

        /// <summary>
        /// Constructor for reference of stock. Used to make Copies of a stock
        /// </summary>
        /// <param name="s">The stock to reference</param>
        public Stock(Stock s)
        {
            this.name = s.name;
            this.price = s.price;
            this.quantity = s.quantity;
        }
    }
}
