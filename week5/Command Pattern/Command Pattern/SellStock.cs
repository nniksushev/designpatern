﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Pattern
{
    /// <summary>
    /// Concrete class implementing Order
    /// </summary>
    public class SellStock : IOrder
    {
        protected Stock stock;//The stock 
        protected Stock oldStock;//The previous state of the stock 
        protected int toSell;//The ammount to sell

        /// <summary>
        /// Constructor for sell stock
        /// </summary>
        /// <param name="_stock">The stock from which will be selling</param>
        /// <param name="ammount">The ammount to be sold</param>
        public SellStock(Stock _stock, int ammount)
        {
            this.ToChange = ammount;
            stock = _stock;
            oldStock = new Stock(stock);
        }

        /// <summary>
        /// Executes the stock Sell
        /// </summary>
        public override void Execute()
        {
            oldStock = new Stock(stock);
            stock.Sell(ToChange);
        }

        /// <summary>
        /// Returns the stock to the previous state
        /// </summary>
        public override  void Undo()
        {
            oldStock = new Stock(stock);
            stock.Buy(ToChange);
        }

        /// <summary>
        /// Returns a string of the sell stock in format Sell {Change}: {Stock.ToString()}
        /// </summary>
        /// <returns>The string of the order</returns>
        public override string ToString()
        {
            return "Sell "+ ToChange +" : " + oldStock.ToString();
        }
    }
}
