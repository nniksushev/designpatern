﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Command_Pattern
{
    public partial class StockSystem : Form
    {

        List<Stock> stocks;

        Broker broker;
        public StockSystem()
        {
            InitializeComponent();

            InitBroker();
            InitStock();
            InitLists();
            btnBuy.Enabled = false;
            btnSell.Enabled = false;
        }

        private void InitBroker()
        {
            broker = new Broker();
            broker.OnOrdersExecuted += () => UpdateExecutedList();
            broker.OnOrderUndone += () => UpdateExecutedList();
            broker.OnOrderQueued += () => UpdateQueueList();
        }

        private void InitStock()
        {
            stocks = new List<Stock>();
            Stock nasdaq, dow, sAndp, chipotle;
            nasdaq = new Stock("nasdaq", 20, 300);

            dow = new Stock("dow", 20, 500);

            sAndp = new Stock("s&p", 50, 20);

            chipotle = new Stock("chipotle", 5000, 5);
            stocks.Add(nasdaq);
            stocks.Add(dow);
            stocks.Add(sAndp);
            stocks.Add(chipotle);
        }

        private void InitLists()
        {
            //List how to look like
            listExecuted.Columns.Add("Order details");

            listExecuted.View = View.Details;
            SizeLastColumn(listExecuted);

            listToBeApproved.Columns.Add("Order details");

            listToBeApproved.View = View.Details;
            SizeLastColumn(listToBeApproved);

            listStockView.Columns.Add("Order details");

            listStockView.View = View.Details;
            SizeLastColumn(listStockView);
            //End of how lists to look like

            //Fill general list of stock
            ListViewItem itm = new ListViewItem();
            Stock nasdaq, dow, sAndp, chipotle;

            nasdaq = stocks[0];
            itm.Text = nasdaq.ToString();
            itm.Tag = nasdaq;
            listStockView.Items.Add(itm);

            itm = new ListViewItem();
            dow = stocks[1];
            itm.Text = dow.ToString();
            itm.Tag = dow;
            listStockView.Items.Add(itm);

            itm = new ListViewItem();
            sAndp = stocks[2];
            itm.Text = sAndp.ToString();
            itm.Tag = sAndp;
            listStockView.Items.Add(itm);

            itm = new ListViewItem();
            chipotle = stocks[3];
            itm.Text = chipotle.ToString();
            itm.Tag = chipotle;
            listStockView.Items.Add(itm);

        }

        /// <summary>
        /// Resize last column size
        /// </summary>
        /// <param name="lv">The listview to resize</param>
        private void SizeLastColumn(ListView lv)
        {
            lv.Columns[lv.Columns.Count - 1].Width = -2;
        }

        /// <summary>
        /// Update the queue list of orders
        /// </summary>
        private void UpdateQueueList()
        {
            IOrder[] orders = broker.GetQueuedOrders();
            UpdateList(listToBeApproved, orders);
        }


        private void btnBuy_Click(object sender, EventArgs e)
        {
            int ammount = (int)numericToChange.Value;//Gets the ammount to buy
            Stock s = (Stock)listStockView.SelectedItems[0].Tag; //Gets the stock from the list
            BuyStock buyStockOrder = new BuyStock(s, ammount); //New buy stock order

            //Enqueue the order
            broker.TakeOrder(buyStockOrder);
        }

        private void btnSell_Click(object sender, EventArgs e)
        {
            int ammount = (int)numericToChange.Value; //Gets the ammount to sell
            Stock s = (Stock)listStockView.SelectedItems[0].Tag; //Gets the stock from the list
            SellStock sellStockOrder = new SellStock(s, ammount);//New sell stock order

            //Enqueue the order
            broker.TakeOrder(sellStockOrder);
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            //Undo the last order
            broker.UndoOrder();
        }

        private void StockSystem_Load(object sender, EventArgs e)
        {

        }

        private void butPlace_Click(object sender, EventArgs e)
        {
            //Execute all orders
            broker.PlaceOrders();

        }

        /// <summary>
        /// Update the list of executed orders
        /// </summary>
        public void UpdateExecutedList()
        {
            UpdateList(listExecuted, broker.GetExecutedOrders());//Updates the list of executed orders
            UpdateList(listToBeApproved, broker.GetQueuedOrders()); // updates the list of queued order


            //updates the stock list
            listStockView.Items.Clear();
            foreach (Stock item in stocks)
            {
                ListViewItem lv = new ListViewItem();
                lv.Text = item.ToString();
                lv.Tag = (Stock)item;
                listStockView.Items.Add(lv);
            }
            btnBuy.Enabled = false;
            btnSell.Enabled = false;
        }

        /// <summary>
        /// Updates a listview with orders
        /// </summary>
        /// <param name="listView">The listview to update</param>
        /// <param name="orders">The orders to fill in the list</param>
        private void UpdateList(ListView listView, IOrder[] orders)
        {

            listView.Items.Clear();

            foreach (IOrder order in orders)
            {
                listView.Items.Add(order.ToString());
            }
            this.Invoke(new Action(() =>
            {
                //Resize based on content
                listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
           ));




        }

        private void butClear_Click(object sender, EventArgs e)
        {
            broker.ClearOrders();//clear all orders
        }

        private void listStockView_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Make sure there is a selected item before buying.selling
            if (listStockView.SelectedItems.Count > 0)
            {
                btnBuy.Enabled = true;
                btnSell.Enabled = true;
            }
            else
            {
                btnBuy.Enabled = false;
                btnSell.Enabled = false;
            }
        }
    }
}
