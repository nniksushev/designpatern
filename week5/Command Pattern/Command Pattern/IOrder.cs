﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Pattern
{
    /// <summary>
    /// Command interface
    /// </summary>
    public abstract class IOrder
    {
      

        /// <summary>
        /// Ammount to change for the order
        /// </summary>
        public int ToChange
        {
            get;
            set;
        }

        /// <summary>
        /// Execute method
        /// </summary>

        public virtual  void Execute() { }
        /// <summary>
        /// Undo
        /// </summary>
         public virtual void Undo() { }
    }
}
