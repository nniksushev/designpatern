﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Pattern
{
    /// <summary>
    /// Command Invoker class
    /// </summary>
    public class Broker
    {        
        private Queue<IOrder> orderList;//List of queued orders
        private Stack<IOrder> executed;//List of executed orders

        public event Action OnOrdersExecuted = () => { }; //Action to update the executed list
        public event Action OnOrderUndone = () => { }; // Action to update when there is an un-done 
        public event Action OnOrderQueued = () => { }; //Action to update when there is a new queued order


        /// <summary>
        /// Constructor for Broker
        /// </summary>
        public Broker()
        {
            orderList = new Queue<IOrder>();
            executed = new Stack<IOrder>();
        }

        /// <summary>
        /// Enqueue a new order and send OnOrderQueue 
        /// </summary>
        /// <param name="_order">The order to enqueue</param>
        public void TakeOrder(IOrder _order)
        {
            orderList.Enqueue(_order);
            OnOrderQueued();
        }
        /// <summary>
        /// Executes all the orders. If there are no orders avaiable sends an update to the orders.
        /// If there are executes all and pushesh the executed orders to the stack of executed orders.
        /// </summary>
        public void PlaceOrders()
        {
            
            while (orderList.Count > 0)
            {

                IOrder order = orderList.Dequeue();//Gets the next order
                //Try catch for when there is not enough quantity when selling.
                try
                {
                    order.Execute();
                    executed.Push(order);
                }
                catch (Exception e)
                {
                    System.Windows.Forms.MessageBox.Show(e.Message); //Shows a message if it is not possible to buy
                }
            }
            OnOrdersExecuted();
        }

        /// <summary>
        /// Returns the queued orders into an array
        /// </summary>
        /// <returns>Array of queued orders</returns>
        public IOrder[] GetQueuedOrders()
        {
            return orderList.ToArray();
        }

        /// <summary>
        /// Undoes the last executed order.
        /// </summary>
        public void UndoOrder()
        {
            if (executed.Count > 0)
            {
                IOrder order = executed.Pop();//Gets the last executed order
                order.Undo();//Undoes it
                OnOrderUndone();//Send undone action call
            }
        }

        /// <summary>
        /// Returns the executed Orders into an array
        /// </summary>
        /// <returns>Array of executed orders</returns>
        public IOrder[] GetExecutedOrders()
        {
            return this.executed.ToArray();
        }


        /// <summary>
        /// Clear all current queued orders
        /// </summary>
        public void ClearOrders()
        {
            this.orderList.Clear();
            OnOrderQueued();//Send queue change 
        }
    }
}
