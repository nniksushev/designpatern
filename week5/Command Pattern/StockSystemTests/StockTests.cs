﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Command_Pattern;
namespace StockSystemTests
{
    [TestClass]
    public class StockTests
    {
        [TestMethod]
        public void CreateStock()
        {
            Stock s = new Stock("Stock", 20, 20);
            
            String expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 20, 20);
            Assert.AreEqual(expected, s.ToString());
        }

        [TestMethod]
        public void StockBuy()
        {
            Stock s = new Stock("Stock", 20, 20);

            String expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 20, 20);
            Assert.AreEqual(expected, s.ToString());
            s.Buy(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 25, 20);
            Assert.AreEqual(expected, s.ToString());

            s.Buy(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 30, 20);
            Assert.AreEqual(expected, s.ToString());

        }

        [TestMethod]
        public void StockSell()
        {
            Stock s = new Stock("Stock", 20, 20);

            String expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 20, 20);
            Assert.AreEqual(expected, s.ToString());
            s.Sell(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 15, 20);
            Assert.AreEqual(expected, s.ToString());

            s.Sell(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 10, 20);
            Assert.AreEqual(expected, s.ToString());

        }

        [TestMethod]
        public void StockSellBuy()
        {
            Stock s = new Stock("Stock", 20, 20);

            String expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 20, 20);
            Assert.AreEqual(expected, s.ToString());
            s.Sell(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 15, 20);
            Assert.AreEqual(expected, s.ToString());

            s.Buy(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 20, 20);
            Assert.AreEqual(expected, s.ToString());

        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException),"insuficient quantity to sell")]
        public void StockSellTooMuch()
        {
            Stock s = new Stock("Stock", 20, 20);

            String expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 20, 20);
            Assert.AreEqual(expected, s.ToString());
            s.Sell(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 15, 20);
            Assert.AreEqual(expected, s.ToString());

            s.Sell(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 10, 20);
            Assert.AreEqual(expected, s.ToString());

            s.Sell(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 5, 20);
            Assert.AreEqual(expected, s.ToString());

            s.Sell(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 0, 20);
            Assert.AreEqual(expected, s.ToString());
            s.Sell(5);
            expected = String.Format("Stock [ Name: {0}, Quantity: {1} Price: {2} ]", "Stock", 20, 20);
            Assert.AreEqual(expected, s.ToString());

        }
    }
}
