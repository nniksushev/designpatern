﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Command_Pattern;
namespace StockSystemTests
{
    [TestClass]
    public class BrokerTests
    {
        [TestMethod]
        public void CreateBroker()
        {
            Broker b = new Broker();
            Assert.IsNotNull(b);
        }

        [TestMethod]
        public void TakeOrders()
        {
            Broker b = new Broker();
           
           
            Stock s = new Stock("zippers", 20, 20);
            IOrder order = new SellStock(s, 3);
            b.TakeOrder(order);
            Assert.AreEqual(1, b.GetQueuedOrders().Length);
            order = new SellStock(s, 3);
            b.TakeOrder(order);
            order = new SellStock(s, 3);
            b.TakeOrder(order);

            order = new BuyStock(s, 3);
            b.TakeOrder(order);
            order = new BuyStock(s, 3);
            b.TakeOrder(order);
            Assert.AreEqual(5, b.GetQueuedOrders().Length);
        }

        [TestMethod]
        public void ExecuteOrders()
        {
            //should have no unexecuted orders at the end
            Broker b = new Broker();


            Stock s = new Stock("zippers", 20, 20);
            IOrder order = new SellStock(s, 3);
            b.TakeOrder(order);
            Assert.AreEqual(1, b.GetQueuedOrders().Length);
            order = new SellStock(s, 3);
            b.TakeOrder(order);
            order = new SellStock(s, 3);
            b.TakeOrder(order);

            order = new BuyStock(s, 3);
            b.TakeOrder(order);
            order = new BuyStock(s, 3);
            b.TakeOrder(order);
            Assert.AreEqual(5, b.GetQueuedOrders().Length);

            b.PlaceOrders();
            Assert.AreEqual(5, b.GetExecutedOrders().Length);
            Assert.AreEqual(0, b.GetQueuedOrders().Length);
        }

        [TestMethod]
        public void ExecuteOrdersAndUndo()
        {
            Broker b = new Broker();


            Stock s = new Stock("zippers", 20, 20);
            IOrder order = new SellStock(s, 3);
            b.TakeOrder(order);
            Assert.AreEqual(1, b.GetQueuedOrders().Length);
            order = new SellStock(s, 3);
            b.TakeOrder(order);
            order = new SellStock(s, 3);
            b.TakeOrder(order);

            order = new BuyStock(s, 3);
            b.TakeOrder(order);
            order = new BuyStock(s, 3);
            b.TakeOrder(order);
            Assert.AreEqual(5, b.GetQueuedOrders().Length);

            b.PlaceOrders();
            Assert.AreEqual(5, b.GetExecutedOrders().Length);
            Assert.AreEqual(0, b.GetQueuedOrders().Length);
            //5 orders executed, unexecuted 3 should have 2
            b.UndoOrder();
            b.UndoOrder();
            b.UndoOrder();
            Assert.AreEqual(2, b.GetExecutedOrders().Length);
        }

        [TestMethod]
        public void ExecuteAndClear()
        {
            Broker b = new Broker();


            Stock s = new Stock("zippers", 20, 20);
            IOrder order = new SellStock(s, 3);
            b.TakeOrder(order);
            Assert.AreEqual(1, b.GetQueuedOrders().Length);
            order = new SellStock(s, 3);
            b.TakeOrder(order);
            order = new SellStock(s, 3);
            b.TakeOrder(order);
            b.ClearOrders();
            order = new BuyStock(s, 3);
            b.TakeOrder(order);
            order = new BuyStock(s, 3);
            b.TakeOrder(order);
            Assert.AreEqual(2, b.GetQueuedOrders().Length);

            //GetQueuedOrders.length = 2
            b.PlaceOrders();
            //GetQueuedOrders.length = 0
            Assert.AreEqual(2, b.GetExecutedOrders().Length);
            
            Assert.AreEqual(0, b.GetQueuedOrders().Length);

        }
    }
}
