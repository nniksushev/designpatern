﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DecoratorPattern2
{
    public partial class Form1 : Form
    {
        int radiobutton;
        Order order;

        public Form1()
        {
            InitializeComponent();
        }

        private void rbphoto_CheckedChanged(object sender, EventArgs e)
        {
            radiobutton = 0;
        }

        private void rbvideo_CheckedChanged(object sender, EventArgs e)
        {
            radiobutton = 1;
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
                switch (radiobutton)
                {
                    case 0:
                        order = new PhotoCamera();
                        for (int i = 0; i < nudLens.Value; i++)
                        {
                            order = new Lens(order);
                        }
                        for (int i = 0; i < nudFilter.Value; i++)
                        {
                            order = new Filter(order);
                        }
                        for (int i = 0; i < nudExtender.Value; i++)
                        {
                            order = new Extender(order);
                        }

                        break;
                    case 1:
                        order = new VideoCamera();
                        for (int i = 0; i < nudLens.Value; i++)
                        {
                            order = new Lens(order);
                        }
                        for (int i = 0; i < nudFilter.Value; i++)
                        {
                            order = new Filter(order);
                        }
                        for (int i = 0; i < nudExtender.Value; i++)
                        {
                            order = new Extender(order);
                        }

                        break;
                }

                lbprice.Text = order.cost().ToString() + " €";
        }
    }
}
